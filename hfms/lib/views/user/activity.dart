import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../scoped-models/main_scope.dart';
import '../widgets/simplebar_chart.dart';
import '../widgets/reading_chart.dart';

class ActivityView extends StatelessWidget {
  final String activity;
  final String title;

  ActivityView(this.activity, this.title);

  Widget _buildLineChart(BuildContext context, MainModel model) {
    List<List<double>> values = [];
    List<String> names = [];
    List<dynamic> colors = [];

    int numReports = model.readings.length > 4 ? 5 : model.readings.length;
    int added = 0;

    for (var i = 0; i < model.readings.length && added < numReports; i++) {
      if (model.readings[i].activity == activity && model.readings[i].processed) {
        added++;
        colors.add(model.readings[i].color);
        values.add(model.readings[i].heartRate);
        names.add((model.readings[i].timestamp.substring(0, 10) +
            " " +
            model.readings[i].timestamp.substring(11, 16)));
      }
    }

    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth * 0.90;

    if (added > 0) {
      return Container(
          height: targetWidth * 0.7,
          width: targetWidth,
          child: SimpleLineChart.withMultipleData(values, names, colors));
    } else {
      return Container(
        height: targetWidth * 0.7,
        width: targetWidth,
        child: Center(
          child: Text(
            "You have not taken any reading for this activity",
            style: TextStyle(
              fontStyle: FontStyle.italic,
            ),
          ),
        ),
      );
    }
  }

  Widget _buildContent(BuildContext context, MainModel model) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.all(deviceHeight * 0.02),
      children: <Widget>[
        SizedBox(
          height: deviceHeight * 0.02,
        ),
        Container(
          alignment: Alignment.center,
          child: Icon(
              activity == 'woke'
                  ? Icons.airline_seat_individual_suite
                  : activity == 'exercise'
                      ? Icons.fitness_center
                      : activity == 'alcohol'
                          ? Icons.smoking_rooms
                          : Icons.event_seat,
              size: deviceHeight * 0.1,
              color: Colors.black45),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            title,
            style: TextStyle(
                fontSize: deviceHeight * 0.025,
                color: Theme.of(context).primaryColor),
          ),
        ),
        SizedBox(
          height: deviceHeight * 0.03,
        ),
        Text(
          'How does this activity affect your heart rate?',
          style:
              TextStyle(fontSize: deviceHeight * 0.021, color: Colors.black54),
          textAlign: TextAlign.left,
        ),
        SizedBox(
          height: deviceHeight * 0.01,
        ),
        Text(
          activity == 'exercise'
              ? "Exercise increases the rate at which energy is needed from food, increasing the need for both food and oxygen by the body. This is why when we exercise both pulse/heart rate and breathing rate increase. The heart speeds up to pump extra food and oxygen to the muscles, while breathing speeds up to get more oxygen into the body and remove carbon dioxide."
              : activity == 'resting'
                  ? "Your heart rate changes from minute to minute. It depends on whether you are standing up or lying down, moving around or sitting still, stressed or relaxed. Your resting heart rate, though, tends to be stable from day to day. The usual range for resting heart rate is anywhere between 60 and 90 beats per minute. Above 90 is considered high."
                  : activity == 'alcohol'
                      ? "The ingestion of harmful substances, particularly drugs and alcohol, may be associated with deterioration in the health of the heart and/or blood vessels3. This is known as cardiovascular disease, and may have considerable impacts on overall health and longevity."
                      : "Your body undergoes a number of important recuperative processes during sleep, from cell renewal to muscle repair to restorative brain activity. Changes in breathing, oxygen levels, and heart rate occur as well. These actions are essential to your long-term health. While having a slight fluctuation in heart rate during sleep is normal, it is important to understand the causes of more noticeable spikes in your heart’s number of beats per minute.",
          style: TextStyle(fontSize: deviceHeight * 0.02),
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: deviceHeight * 0.03,
        ),
        Text(
          'Unit of measurement',
          style:
              TextStyle(fontSize: deviceHeight * 0.021, color: Colors.black54),
          textAlign: TextAlign.left,
        ),
        SizedBox(
          height: deviceHeight * 0.01,
        ),
        Text(
          'beats per minute (bpm)',
          style: TextStyle(fontSize: deviceHeight * 0.02),
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: deviceHeight * 0.03,
        ),
        Text(
          'Average measurement',
          style:
              TextStyle(fontSize: deviceHeight * 0.021, color: Colors.black54),
          textAlign: TextAlign.left,
        ),
        SizedBox(
          height: deviceHeight * 0.01,
        ),
        Text(
          model.avgHRByActivity[activity] == 0
              ? "You have not taken any reading for this activity"
              : model.avgHRByActivity[activity].toString() + ' bpm',
          style: TextStyle(
              fontSize: deviceHeight * 0.02,
              fontStyle: model.avgHRByActivity[activity] == 0
                  ? FontStyle.italic
                  : FontStyle.normal),
          textAlign: TextAlign.justify,
        ),
        SizedBox(
          height: deviceHeight * 0.03,
        ),
        Text(
          'Last reported values',
          style:
              TextStyle(fontSize: deviceHeight * 0.021, color: Colors.black54),
          textAlign: TextAlign.left,
        ),
        SizedBox(
          height: deviceHeight * 0.03,
        ),
        _buildLineChart(context, model),
        SizedBox(
          height: deviceHeight * 0.03,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    //buildChartProgressively(reading.plethismogram);
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        title:
            Text('Activity statistics', style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.white,
      ),
      body: ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          return Container(
            child: _buildContent(context, model),
          );
        },
      ),
    );
  }
}
