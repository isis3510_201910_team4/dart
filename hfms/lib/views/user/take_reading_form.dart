import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import './take_reading.dart';
import '../../scoped-models/main_scope.dart';

class TakeReadingForm extends StatefulWidget {
  final MainModel model;

  TakeReadingForm(this.model);

  @override
  State<StatefulWidget> createState() {
    return _TakeReadingFormState();
  }
}

class _TakeReadingFormState extends State<TakeReadingForm> {
  int _currentStep = 0;
  String _selectedActivity = 'resting';
  final TextEditingController _moreInfoController = TextEditingController();
  final FocusNode _textFieldFocusNode = FocusNode();

  File _videoFile;

  Widget _buildActivityPicker() {
    return DropdownButton<String>(
      isExpanded: true,
      value: _selectedActivity,
      items: [
        DropdownMenuItem<String>(value: 'resting', child: Text('Resting?')),
        DropdownMenuItem<String>(value: 'woke', child: Text('Just waking up?')),
        DropdownMenuItem<String>(value: 'exercise', child: Text('Exercising?')),
        DropdownMenuItem<String>(
            value: 'alcohol', child: Text('Taking drugs or alcohol?')),
      ],
      onChanged: (value) {
        setState(() {
          _selectedActivity = value;
        });
      },
    );
  }

  Widget _buildMoreInfoTextField() {
    return TextField(
      maxLength: 128,
      focusNode: _textFieldFocusNode,
      controller: _moreInfoController,
      decoration: InputDecoration(
        labelText: 'Write about it here!',
        counterText: '',
      ),
      maxLines: null,
    );
  }

  void _submitForm() async {
    final Map<String, dynamic> result = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) {
          return TakeReadingView(widget.model, _videoFile, _selectedActivity,
              _moreInfoController.text);
        },
      ),
    );

    if (ModalRoute.of(context).isCurrent) {
      Navigator.of(context).pop(result);
    }
  }

  List<Step> _buildSteps() {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth * 0.6;
    final double deviceHeight = MediaQuery.of(context).size.height;
    double _fontSize = deviceHeight * 0.021;
    return [
      Step(
        state: _currentStep == 0 ? StepState.editing : StepState.complete,
        title: Text(
          'Were you...',
          style: TextStyle(fontSize: _fontSize),
        ),
        content: _buildActivityPicker(),
        isActive: _currentStep == 0,
      ),
      Step(
        state: _currentStep == 1
            ? StepState.editing
            : _currentStep == 0 ? StepState.indexed : StepState.complete,
        title: Text(
          'More information',
          style: TextStyle(fontSize: _fontSize),
        ),
        subtitle: Container(
          width: targetWidth,
          child: Text(
            'Want to add further details to this reading?',
            style: TextStyle(fontSize: _fontSize),
          ),
        ),
        content: _buildMoreInfoTextField(),
        isActive: _currentStep == 1,
      ),
      Step(
        state: _currentStep >= 2
            ? _videoFile == null ? StepState.editing : StepState.complete
            : StepState.indexed,
        title: Text(
          'Take a video of yourself',
          style: TextStyle(fontSize: _fontSize),
        ),
        content: Column(
          children: <Widget>[
            Text(
                'Remember to hold the phone steady in front of you.\nTry to have good lighting.\nRecord your face in the center of the video.'),
            SizedBox(height: 20.0),
            FlatButton(
              onPressed: () => _openVideoPicker(context),
              child: _videoFile == null
                  ? Text('Take video')
                  : Text('Change or delete video'),
              color: Colors.grey,
              textColor: Colors.white,
            ),
            SizedBox(height: 20.0),
          ],
        ),
        isActive: _currentStep == 2,
      ),
      Step(
        state: _videoFile == null ? StepState.disabled : StepState.indexed,
        title: Text(
          'Submit video',
          style: TextStyle(fontSize: _fontSize),
        ),
        content: Text(
            'Your video is ready to be send. You will see the results when we finish processing it.'),
        isActive: _currentStep == 3 && _videoFile != null,
      ),
    ];
  }

  Widget _buildStepper() {
    return Stepper(
      controlsBuilder: (BuildContext context,
          {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
        return Row(
          children: <Widget>[
            FlatButton(
              onPressed: _videoFile == null && _currentStep >= 2
                  ? null
                  : onStepContinue,
              child: _currentStep < 3 ? Text('CONTINUE') : Text('SUBMIT'),
              color: _videoFile == null && _currentStep >= 2
                  ? Colors.white
                  : Theme.of(context).buttonColor,
              textColor: _videoFile == null && _currentStep >= 2
                  ? Colors.grey
                  : Colors.white,
            ),
            FlatButton(
              onPressed: onStepCancel,
              child: Text('BACK'),
              textColor: Theme.of(context).buttonColor,
            ),
          ],
        );
      },
      currentStep: _currentStep,
      onStepTapped: (stepTapped) {
        setState(() {
          _currentStep = stepTapped;
          // FocusScope.of(context).requestFocus(
          //     stepTapped == 1 ? _textFieldFocusNode : FocusNode());
        });
      },
      onStepContinue: () {
        setState(() {
          if (_currentStep == 0) {
            _currentStep += 1;
            // FocusScope.of(context).requestFocus(_textFieldFocusNode);
          } else if (_currentStep == 1) {
            FocusScope.of(context).requestFocus(FocusNode());
            _currentStep += 1;
          } else if (_currentStep == 2) {
            _currentStep += 1;
            // _openVideoPicker(context);
          } else {
            if (_videoFile != null) {
              _submitForm();
            }
          }
        });
      },
      onStepCancel: () {
        setState(() {
          if (_currentStep == 0) {
            print('We just started!');
          } else if (_currentStep == 1) {
            FocusScope.of(context).requestFocus(FocusNode());
            _currentStep -= 1;
          } else {
            // FocusScope.of(context).requestFocus(_textFieldFocusNode);
            _currentStep -= 1;
          }
        });
      },
      steps: _buildSteps(),
    );
  }

  void _openVideoPicker(BuildContext context) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height:
                _videoFile == null ? deviceHeight * 0.25 : deviceHeight * 0.30,
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: deviceHeight * 0.01,
                ),
                Text(
                  'Pick a video',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: deviceHeight * 0.025),
                ),
                SizedBox(
                  height: deviceHeight * 0.01,
                ),
                FlatButton(
                  child: Text(
                    'Use camera',
                    style: TextStyle(fontSize: deviceHeight * 0.02),
                  ),
                  onPressed: () {
                    _getVideo(context, ImageSource.camera);
                  },
                ),
                FlatButton(
                  child: Text(
                    'Choose from gallery',
                    style: TextStyle(fontSize: deviceHeight * 0.02),
                  ),
                  onPressed: () {
                    _getVideo(context, ImageSource.gallery);
                  },
                ),
                _videoFile == null
                    ? Text('')
                    : FlatButton(
                        child: Text(
                          'Delete',
                          style: TextStyle(fontSize: deviceHeight * 0.02),
                        ),
                        onPressed: () {
                          _deleteVideo();
                        },
                      )
              ],
            ),
          );
        });
  }

  void _deleteVideo() {
    setState(() {
      _videoFile = null;
    });
    Navigator.pop(context);
  }

  void _getVideo(BuildContext context, ImageSource source) {
    ImagePicker.pickVideo(source: source).then((File video) {
      setState(() {
        _videoFile = video;
      });
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        title:
            Text('Take a new reading', style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.white,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Center(child: _buildStepper()),
      ),
    );
  }
}
