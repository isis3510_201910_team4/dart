import 'package:flutter/material.dart';

import './home.dart';
import './profile.dart';
import './reports.dart';
import '../../scoped-models/main_scope.dart';

class BrowserViewUser extends StatefulWidget {
  final MainModel model;
  final int selectedIndex;
  BrowserViewUser(this.model, {this.selectedIndex = 0});

  @override
  State<StatefulWidget> createState() {
    return _BrowserViewState(selectedIndex);
  }
}

class _BrowserViewState extends State<BrowserViewUser> {
  int selectedIndex;
  _BrowserViewState(this.selectedIndex);

  @override
  initState() {
    widget.model.fetchRelations();
    widget.model.fetchReadings(widget.model.authenticatedUser.id);
    super.initState();
  }

  final _widgetOptions = [
    HomeTab(),
    ReportsTab(),    
    ProfileTab(),
  ];

  Widget _buildBottomNavigationBar() {
    return BottomNavigationBar(
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Home'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.event_note),
          title: Text('Reports'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          title: Text('Profile'),
        ),
      ],
      currentIndex: selectedIndex,
      onTap: _onItemTapped,
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 2.0,
        title: Text(
          'HeartU',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Center(
        child: _widgetOptions.elementAt(selectedIndex),
      ),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }
}
