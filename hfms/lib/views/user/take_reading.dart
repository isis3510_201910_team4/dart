import 'package:flutter/material.dart';
import 'dart:io';
import 'package:scoped_model/scoped_model.dart';

import '../../scoped-models/main_scope.dart';

class TakeReadingView extends StatefulWidget {
  final MainModel model;
  final File video;
  final String activity;
  final String moreInfo;

  TakeReadingView(this.model, this.video, this.activity, this.moreInfo);

  @override
  State<StatefulWidget> createState() {
    return _TakeReadingViewState();
  }
}

class _TakeReadingViewState extends State<TakeReadingView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    
    if (ModalRoute.of(context).isCurrent) {
      widget.model
          .takeReading(widget.video, widget.activity, widget.moreInfo)
          .then((Map<String, dynamic> newReading) async {
        if (ModalRoute.of(context).isCurrent) {
          Navigator.of(context).pop(newReading);
        }
      });
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 2.0,
        title: Text(
          'HeartU',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(50.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Sending your video...',
              style: TextStyle(fontSize: 22.0),
            ),
            SizedBox(
              height: 20.0,
            ),
            ScopedModelDescendant<MainModel>(
                builder: (BuildContext context, Widget child, MainModel model) {
              return LinearProgressIndicator(
                value: model.sendingVideoPercent,
                semanticsLabel: 'Sending your video...',
                semanticsValue: "%",
                backgroundColor: Colors.grey.shade300,
              );
            }),
          ],
        ),
      ),
    );
  }
}
