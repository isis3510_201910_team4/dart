import 'package:flutter/material.dart';
import '../../models/reading.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../scoped-models/main_scope.dart';
import '../../models/comment.dart';
import '../widgets/reading_chart.dart';

class ReadingView extends StatelessWidget {
  final TextEditingController _newCommentController = TextEditingController();
  final FocusNode _textFieldFocusNode = FocusNode();
  final ScrollController _scrollController = ScrollController();

  final Reading reading;
  final color;

  ReadingView(this.reading, this.color);

  Widget _buildChart(List<double> data, BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth * 0.90;
    return Container(
        height: targetWidth * 0.7,
        width: targetWidth,
        child: SimpleLineChart.withData(data, color));
  }

  Widget _buildStatisticLine(String name, String value, BuildContext context) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            alignment: Alignment.centerLeft,
            child: Text(
              name,
              style: TextStyle(
                  fontSize: deviceHeight * 0.025,
                  color: Theme.of(context).primaryColor),
            ),
          ),
        ),
        Expanded(
          child: Container(
            alignment: Alignment.centerRight,
            child: Text(
              value,
              textAlign: TextAlign.right,
              style: TextStyle(
                fontSize: deviceHeight * 0.02,
                color: Colors.black54,
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildComments(BuildContext context, MainModel model) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    final double deviceWidth = MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: TextField(
                  maxLength: 256,
                  focusNode: _textFieldFocusNode,
                  controller: _newCommentController,
                  decoration: InputDecoration(
                    labelText: 'Please enter a new comment here',
                    labelStyle: TextStyle(fontSize: deviceWidth * 0.04),
                    counterText: '',
                  ),
                  maxLines: null,
                ),
              ),
              SizedBox(
                width: deviceWidth * 0.04,
              ),
              RaisedButton(
                child: Text(
                  'Send',
                  style: TextStyle(
                      fontSize: deviceWidth * 0.04, color: Colors.white),
                ),
                onPressed: () {
                  if (_newCommentController.text.trim().length > 0) {
                    model
                        .addComment(_newCommentController.text.trim())
                        .then((_) {
                      _newCommentController.clear();
                    });
                  } else {
                    _newCommentController.clear();
                  }
                },
              )
            ],
          ),
          SizedBox(
            height: deviceHeight * 0.02,
          ),
          Expanded(
            child: ListView.builder(
              physics: AlwaysScrollableScrollPhysics(),
              controller: _scrollController,
              padding: EdgeInsets.all(deviceHeight * 0.01),
              reverse: false,
              itemBuilder: (BuildContext context, int i) {
                Widget card;
                Comment comment = model.commentsList[i];
                if (comment.userId == model.authenticatedUser.id &&
                    comment.doctor) {
                  card = _buildSingleComment(comment, Icons.local_hospital,
                      Alignment.centerRight, deviceHeight, deviceWidth, true);
                } else if (comment.userId == model.authenticatedUser.id &&
                    !comment.doctor) {
                  card = _buildSingleComment(comment, Icons.person,
                      Alignment.centerRight, deviceHeight, deviceWidth, true);
                } else if (comment.doctor) {
                  card = _buildSingleComment(comment, Icons.local_hospital,
                      Alignment.centerLeft, deviceHeight, deviceWidth, false);
                } else {
                  card = _buildSingleComment(comment, Icons.person,
                      Alignment.centerLeft, deviceHeight, deviceWidth, false);
                }
                return card;
              },
              itemCount: model.commentsList.length,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildSingleComment(Comment comment, IconData icon,
      Alignment alignment, double deviceHeight, double deviceWidth, bool me) {
    Widget title;
    if (comment.content.length <= 30) {
      title = Container(
          child: Chip(
            label: Text(
              comment.content,
              softWrap: true,
              maxLines: (comment.content.length / 30 + 1).round(),
              style: TextStyle(fontSize: deviceWidth * 0.04),
            ),
          ),
          alignment: alignment);
    } else {
      title = Container(
          margin: EdgeInsets.fromLTRB(
              0, deviceHeight * 0.01, 0, deviceHeight * 0.01),
          padding: EdgeInsets.all(deviceHeight * 0.01),
          decoration: new BoxDecoration(
            color: Colors.grey.shade300,
            borderRadius:
                new BorderRadius.all(Radius.circular(deviceHeight * 0.02)),
          ),
          child: Text(
            comment.content,
            softWrap: true,
            maxLines: (comment.content.length / 30 + 1).round(),
            style: TextStyle(fontSize: deviceWidth * 0.04),
          ),
          alignment: alignment);
    }
    return ListTile(
      key: Key(comment.id),
      trailing: me ? Icon(icon) : null,
      leading: me ? null : Icon(icon),
      title: title,
      subtitle: Text(
        me ? "Me" : comment.userName,
        textAlign: me ? TextAlign.end : TextAlign.start,
      ),
    );
  }

  Widget _buildEmptyComments(BuildContext context, MainModel model) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Expanded(
                child: TextField(
                  maxLength: 256,
                  focusNode: _textFieldFocusNode,
                  controller: _newCommentController,
                  decoration: InputDecoration(
                    labelText: 'Please enter a new comment here',
                    labelStyle: TextStyle(fontSize: deviceHeight * 0.02),
                    counterText: '',
                  ),
                  maxLines: null,
                ),
              ),
              SizedBox(
                width: deviceHeight * 0.02,
              ),
              RaisedButton(
                child: Text(
                  'Send',
                  style: TextStyle(
                      fontSize: deviceHeight * 0.02, color: Colors.white),
                ),
                onPressed: () {
                  if (_newCommentController.text.trim().length > 0) {
                    model
                        .addComment(_newCommentController.text.trim())
                        .then((_) {
                      _newCommentController.clear();
                    });
                  } else {
                    _newCommentController.clear();
                  }
                },
              )
            ],
          ),
          padding: EdgeInsets.all(20.0),
        ),
        Expanded(
          child: Center(
            child: Text(
              "There are no comments at this reading yet",
              style: TextStyle(
                fontSize: deviceHeight * 0.02,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        SizedBox(
          height: deviceHeight * 0.05,
        )
      ],
    );
  }

  Widget _buildContent(BuildContext context, MainModel model) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.all(deviceHeight * 0.02),
      children: <Widget>[
        SizedBox(
          height: deviceHeight * 0.02,
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            'Heart Rate',
            style: TextStyle(
                fontSize: deviceHeight * 0.025,
                color: Theme.of(context).primaryColor),
          ),
        ),
        SizedBox(
          height: deviceHeight * 0.02,
        ),
        _buildChart(reading.heartRate, context),
        SizedBox(
          height: deviceHeight * 0.04,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _buildStatisticLine(
                'Activity',
                reading.activity == 'exercise'
                    ? "Exercising"
                    : reading.activity == 'resting'
                        ? "Resting"
                        : reading.activity == 'alcohol'
                            ? "Taking drugs or alcohol"
                            : "Just waking up",
                context),
            SizedBox(
              height: deviceHeight * 0.02,
            ),
            _buildStatisticLine('Average Heart Rate',
                reading.averageHeartRate.toStringAsFixed(3), context),
            SizedBox(
              height: deviceHeight * 0.02,
            ),
            _buildStatisticLine('More info:',
                reading.moreInfo.length > 0 ? reading.moreInfo : "-", context),
            SizedBox(
              height: deviceHeight * 0.02,
            ),
            _buildStatisticLine(
                'Behavior',
                reading.averageHeartRate <
                        model.avgHRByActivity[reading.activity] * 0.9
                    ? "Lower than average for this activity"
                    : reading.averageHeartRate >
                            model.avgHRByActivity[reading.activity] * 1.1
                        ? "Higher than average for this activity"
                        : "On average",
                context),
            SizedBox(
              height: deviceHeight * 0.04,
            ),
            Center(
              child: Text(
                reading.locale,
                style: TextStyle(fontSize: deviceHeight * 0.023),
              ),
            ),
            SizedBox(
              height: 5.0,
            ),
            Center(
              child: Text(
                reading.timestamp.substring(0, 10) +
                    " " +
                    reading.timestamp.substring(11, 16),
                style: TextStyle(
                    fontSize: deviceHeight * 0.018, color: Colors.black38),
              ),
            ),
            SizedBox(
              height: deviceHeight * 0.05,
            ),
            RaisedButton(
              child: Text(
                'See comments',
                style: TextStyle(
                    fontSize: deviceHeight * 0.02, color: Colors.white),
              ),
              onPressed: () {
                showModalBottomSheet<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return ScopedModelDescendant<MainModel>(
                        builder: (BuildContext contextBS, Widget childBS,
                            MainModel modelBS) {
                          Widget comments;
                          if (model.commentsList.length > 0) {
                            comments = _buildComments(contextBS, modelBS);
                          } else {
                            comments = _buildEmptyComments(contextBS, modelBS);
                          }
                          return comments;
                        },
                      );
                    });
              },
            ),
            SizedBox(
              height: deviceHeight * 0.1,
            )
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    //buildChartProgressively(reading.plethismogram);
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        title: Text('Test Results', style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.white,
      ),
      body: ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
          return Container(
            child: _buildContent(context, model),
          );
        },
      ),
    );
  }
}
