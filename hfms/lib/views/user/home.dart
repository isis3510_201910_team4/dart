import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../scoped-models/main_scope.dart';
import '../widgets/simplebar_chart.dart';
import './activity.dart';

class HomeTab extends StatelessWidget {
  final TextEditingController _textFieldController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildBarChart(BuildContext context, MainModel model) {
    if (model.readingsList.indexWhere((reading) => reading.processed) < 0) {
      return Center(
        child: Text("You don't have reports yet. Create new ones!"),
      );
    }

    List<double> values = [];
    List<String> names = [];
    List<dynamic> colors = [];

    int numReports = model.readings.length > 4 ? 5 : model.readings.length;

    int added = 0;

    for (var i = 0; i < model.readings.length && added < numReports; i++) {
      if (model.readings[i].processed) {
        added++;
        colors.add(model.readings[i].color);
        values.add(model.readings[i].averageHeartRate);
        names.add((model.readings[i].timestamp.substring(0, 10) +
            " " +
            model.readings[i].timestamp.substring(11, 16)));
      }
    }

    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth * 0.90;
    return Container(
        height: targetWidth * 0.7,
        width: targetWidth,
        child: SimpleBarChart.withData(names, values, colors));
  }

  Widget _buildDoctorsList(BuildContext context, MainModel model) {
    Widget doctorsList = Text(
      "You do not have doctors yet",
      textAlign: TextAlign.center,
    );

    List<Widget> relations = [];

    for (var i = 0; i < model.relationsList.length; i++) {
      relations.add(ListTile(
        leading: const Icon(Icons.person),
        key: Key(model.relationsList[i].id.toString()),
        title: Text(model.relationsList[i].firstName +
            ' ' +
            model.relationsList[i].lastName),
        subtitle: Text(model.relationsList[i].email),
      ));
    }

    if (model.relationsList.length > 0) {
      doctorsList = Column(
        children: relations,
      );
    }

    return doctorsList;
  }

  Widget _buildDoctorsRequestsList(BuildContext context, MainModel model) {
    Widget doctorsRequestsList = Text(
      "You do not have doctors yet",
      textAlign: TextAlign.center,
    );

    List<Widget> requests = [];

    for (var i = 0; i < model.requestsList.length; i++) {
      requests.add(ListTile(
        leading: const Icon(Icons.person),
        key: Key(model.requestsList[i].id.toString()),
        title: Text(model.requestsList[i].firstName +
            ' ' +
            model.requestsList[i].lastName),
        subtitle: Text(model.requestsList[i].email),
      ));
    }

    if (model.requestsList.length > 0) {
      doctorsRequestsList = Column(
        children: requests,
      );
    }

    return doctorsRequestsList;
  }

  Widget _buildCard(
      BuildContext context, String name, String subtitle, dynamic color) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    final double targetHeigth = deviceHeight * 0.20;

    return Center(
        child: SizedBox(
      height: targetHeigth,
      child: Card(
        color: color,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                name,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: deviceHeight * 0.025,
                ),
              ),
              SizedBox(
                height: deviceHeight * 0.01,
              ),
              Text(
                subtitle.length > 6 ? subtitle : subtitle,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: deviceHeight * 0.015,
                ),
              ),
            ],
          ),
        ),
      ),
    ));
  }

  Widget _buildActivityCards(BuildContext context, MainModel model) {
    return Column(
      children: <Widget>[
        Row(children: <Widget>[
          Expanded(
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) {
                    return ActivityView('resting', 'Resting');
                  }),
                );
              },
              child: _buildCard(
                  context,
                  'Resting',
                  model.avgHRByActivity['resting'] == 0
                      ? "-"
                      : model.avgHRByActivity['resting'].toStringAsFixed(3) +
                          " bpm",
                  Color(0xFFE6535C)),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) {
                    return ActivityView('woke', 'Just waking up');
                  }),
                );
              },
              child: _buildCard(
                  context,
                  'Just waking up',
                  model.avgHRByActivity['woke'] == 0
                      ? "-"
                      : model.avgHRByActivity['woke'].toStringAsFixed(3) +
                          " bpm",
                  Color(0xFF7AC7A5)),
            ),
          )
        ]),
        Row(children: <Widget>[
          Expanded(
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) {
                    return ActivityView('exercise', 'Exercising');
                  }),
                );
              },
              child: _buildCard(
                  context,
                  'Exercising',
                  model.avgHRByActivity['exercise'] == 0
                      ? "-"
                      : model.avgHRByActivity['exercise'].toStringAsFixed(3) +
                          " bpm",
                  Color(0xFF84C5E7)),
            ),
          ),
          Expanded(
              child: GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (BuildContext context) {
                  return ActivityView('alcohol', 'Taking drugs or alcohol');
                }),
              );
            },
            child: _buildCard(
                context,
                'Taking drugs or alcohol',
                model.avgHRByActivity['alcohol'] == 0
                    ? "-"
                    : model.avgHRByActivity['alcohol'].toStringAsFixed(3) +
                        " bpm",
                Color(0xFF007AFF)),
          )),
        ]),
      ],
    );
  }

  _displayDialog(BuildContext context, MainModel model,
      BuildContext scaffoldContext) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Add a new doctor'),
            content: Form(
              key: _formKey,
              child: TextFormField(
                controller: _textFieldController,
                maxLength: 256,
                decoration: InputDecoration(
                    hintText: "Write the email of your doctor",
                    counterText: ''),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter an email address';
                  }
                  if (!RegExp(
                          r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]{2}(?:[a-z0-9-]*[a-z0-9])?").hasMatch(value)) {
                    return 'Please enter a valid email address';
                  }
                },
                keyboardType: TextInputType.emailAddress,
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text('CANCEL'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new FlatButton(
                child: new Text(
                  'SEND REQUEST',
                  style: TextStyle(color: Colors.grey.shade800),
                ),
                onPressed: () {
                  Widget snackbar;
                  if (!_formKey.currentState.validate()) {
                    return;
                  } else {
                    model
                        .addRelation(_textFieldController.text)
                        .then((int result) {
                      _textFieldController.clear();
                      Navigator.of(context).pop();

                      if (result == -1) {
                        snackbar = SnackBar(
                          content: Text('Doctor already exists in your list'),
                          duration: Duration(seconds: 3),
                        );
                      } else if (result == 0) {
                        snackbar = SnackBar(
                          content: Text('There is no any doctor with that email'),
                          duration: Duration(seconds: 3),
                        );
                      } else {
                        snackbar = SnackBar(
                          content: Text('Doctor request correctly sent'),
                          duration: Duration(seconds: 3),
                        );
                      }

                      Scaffold.of(scaffoldContext).showSnackBar(snackbar);
                    });
                  }
                },
              )
            ],
          );
        });
  }

  Widget _buildHome(BuildContext scaffoldContext) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 30.0),
              Text(
                'Last reports',
                style: TextStyle(
                    fontSize: 25.0, color: Theme.of(context).primaryColor),
              ),
              SizedBox(height: 10.0),
              _buildBarChart(context, model),
              SizedBox(height: 20.0),
              Text(
                'Activities behavior',
                style: TextStyle(
                    fontSize: 25.0, color: Theme.of(context).primaryColor),
              ),
              SizedBox(height: 20.0),
              _buildActivityCards(context, model),
              SizedBox(height: 20.0),
              Text(
                'Your doctors',
                style: TextStyle(
                    fontSize: 25.0, color: Theme.of(context).primaryColor),
              ),
              SizedBox(height: 20.0),
              _buildDoctorsList(context, model),
              SizedBox(height: 20.0),
              Text(
                'Doctors requests sent',
                style: TextStyle(
                    fontSize: 22.0, color: Theme.of(context).primaryColor),
              ),
              SizedBox(height: 10.0),
              _buildDoctorsRequestsList(context, model),
              SizedBox(height: 20.0),
              RaisedButton(
                textColor: Colors.white,
                child: Text('Add a new doctor'),
                onPressed: () =>
                    _displayDialog(context, model, scaffoldContext),
              ),
              SizedBox(height: 50.0),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(child: _buildHome(context)),
    );
  }
}
