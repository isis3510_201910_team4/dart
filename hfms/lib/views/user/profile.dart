import 'package:flutter/material.dart';
import '../../scoped-models/main_scope.dart';
import 'package:scoped_model/scoped_model.dart';

class ProfileTab extends StatelessWidget {

  List<Widget> _buildDiseasesChips(BuildContext context, MainModel model) {
    List<Widget> chips = [];

    model.authenticatedUser.diseases.forEach((element) {
      chips.add(Container(
        child: Chip(
          backgroundColor: Colors.grey.shade400,
          label: Text(
            element,
            style: TextStyle(color: Colors.white),
          ),
        ),
        padding: EdgeInsets.all(2.0),
      ));
    });

    return chips;  
  }

  Widget _buildProfileList(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;

    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return ListView(
          padding: const EdgeInsets.all(30.0),
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  'Profile',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 25.0,
                  ),
                ),
                /* CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://cdn-img.instyle.com/sites/default/files/styles/684xflex/public/1536155340/090518-liam-hemsworth-tout.jpg?itok=bTw3xvsY'),
                  radius: deviceWidth * 0.3,
                ), */
                SizedBox(
                  height: 20,
                ),
                ListTile(
                  leading: const Icon(Icons.account_circle),
                  title: Text(model.authenticatedUser.firstName +
                      " " +
                      model.authenticatedUser.lastName),
                  subtitle: const Text('Name'),
                ),
                ListTile(
                  leading: const Icon(Icons.email),
                  title: Text(model.authenticatedUser.email),
                  subtitle: const Text('Email'),
                ),
                ListTile(
                  leading: const Icon(Icons.date_range),
                  title:
                      Text(model.authenticatedUser.birthdate.substring(0, 10)),
                  subtitle: const Text('Birthdate'),
                ),
                ListTile(
                  leading: const Icon(Icons.place),
                  title: Text(model.authenticatedUser.country),
                  subtitle: const Text('Country'),
                ),
                ListTile(
                  leading: const Icon(Icons.wc),
                  title: Text(model.authenticatedUser.sex),
                  subtitle: const Text('Sex'),
                ),
                ListTile(
                  leading: const Icon(Icons.nature_people),
                  title: Text(model.authenticatedUser.height),
                  subtitle: const Text('Height'),
                ),
                ListTile(
                  leading: const Icon(Icons.fitness_center),
                  title: Text(model.authenticatedUser.weight),
                  subtitle: const Text('Weight'),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Divider(
                  height: 1.0,
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  child: Text(
                    'Diseases',
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Wrap(
                  children: _buildDiseasesChips(context, model),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Divider(
                  height: 1.0,
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  child: Text(
                    'Account',
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
                /* SizedBox(
                  height: 15.0,
                ),
                SwitchListTile(
                  title: Text(
                    'Enable fingerprint log',
                    style: TextStyle(fontSize: 15.0),
                  ),
                  value: MySharedPreferences._ready ? MySharedPreferences._myBool : model.fingerprintLog,
                  onChanged: (bool value) {
                    _changeFingerPrintPreference(value);
                  },
                  secondary: const Icon(Icons.fingerprint),
                ), */
                SizedBox(
                  height: 15.0,
                ),
                RaisedButton(
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    'Logout',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    model.logout();
                    Navigator.of(context).pushReplacementNamed('/');
                  },
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth * 0.9;

    return Container(
      child: _buildProfileList(context),
    );
  }
}