import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import './take_reading_form.dart';
import '../../scoped-models/main_scope.dart';
import '../widgets/reading_card.dart';
import 'package:connectivity/connectivity.dart';

class ReportsTab extends StatelessWidget {
  final ScrollController _scrollController = ScrollController();

  Widget _buildReadingsList() {
    return ScopedModelDescendant(
        builder: (BuildContext context, Widget child, MainModel model) {
      Widget readingCards;
      final double deviceWidth = MediaQuery.of(context).size.width;
      final double targetWidth = deviceWidth > 550.0 ? 50.0 : deviceWidth * 0.1;
      readingCards = Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(60.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Welcome',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                'Tap on the bottom right button to start taking your exam',
                textAlign: TextAlign.center,
              )
            ],
          ));
      if (model.readings.length > 0 && !model.isLoading) {
        // model.sortReadings();
        readingCards = ListView.builder(
          physics: AlwaysScrollableScrollPhysics(),
          controller: _scrollController,
          padding: EdgeInsets.all(targetWidth),
          reverse: false,
          itemBuilder: (BuildContext context, int index) {
            Widget card;

            if (model.readings[index].processed) {
              card = Dismissible(
                confirmDismiss: (DismissDirection direction) {
                  return _buildDeleteConfirmDialog(context);
                },
                key: Key(model.readings[index].timestamp.toString()),
                onDismissed: (DismissDirection direction) {
                  if (direction == DismissDirection.endToStart ||
                      direction == DismissDirection.startToEnd) {
                    model.selectReading(index);
                    model.deleteReading();
                  }
                },
                child: ReadingCard(model.readings[index], index),
              );
            } else {
              card = ReadingCard(model.readings[index], index);
            }

            return card;
          },
          itemCount: model.readings.length,
        );
      } else if (model.isLoading) {
        readingCards = Center(child: CircularProgressIndicator());
      }
      return readingCards;
    });
  }

  Future<bool> _buildDeleteConfirmDialog(BuildContext context) async {
    return await showDialog(
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Delete this reading?'),
            content: Text('Are you sure you wish to delete this reading?'),
            actions: <Widget>[
              FlatButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              FlatButton(
                  child: Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  })
            ],
          );
        },
        context: context);
  }

  void _moveUp(MainModel model) {
    if (model.readings.length > 0 && !model.isLoading) {
      _scrollController.jumpTo(0.0);
    }
  }

  void _takeExam(BuildContext context, MainModel model) async {
    final Map<String, dynamic> result = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) {
          return TakeReadingForm(model);
        },
      ),
    );

    Widget snackbar;

    if (result != null) {
      if (result['locale'] == null ||
          result['latitude'] == null ||
          result['longitude'] == null) {
        snackbar = SnackBar(
          content: Text('Unable to retrieve location data'),
          duration: Duration(seconds: 3),
        );
        Scaffold.of(context).showSnackBar(snackbar);
      }
      model.addReading(result);
      snackbar = SnackBar(
        content: Text('Reading added successfully'),
        duration: Duration(seconds: 2),
      );
      Scaffold.of(context).showSnackBar(snackbar);
      _moveUp(model);
    } else {
      model.cancelSending();
      snackbar = SnackBar(
        content: Text('Exam canceled'),
        duration: Duration(seconds: 3),
      );
      Scaffold.of(context).showSnackBar(snackbar);
    }
  }

  _buildFloatingButton(contextScaffold) {
    final double deviceWidth = MediaQuery.of(contextScaffold).size.width;
    final double deviceHeight = MediaQuery.of(contextScaffold).size.height;
    final double targetWidth =
        deviceWidth > deviceHeight ? deviceWidth * 0.1 : deviceWidth * 0.15;

    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Align(
          child: SizedBox(
            width: targetWidth,
            height: targetWidth,
            child: FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () => {
                    model
                        .checkConnectivityApp()
                        .then((ConnectivityResult result) {
                      if (result == ConnectivityResult.none) {
                        Scaffold.of(contextScaffold).showSnackBar(SnackBar(
                          content: Text('Check your internet connection'),
                          duration: Duration(seconds: 3),
                        ));
                      } else {
                        _takeExam(context, model);
                      }
                    })
                  },
              backgroundColor: Theme.of(context).accentColor,
            ),
          ),
          alignment: FractionalOffset(0.9, 0.95),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[_buildReadingsList(), _buildFloatingButton(context)],
    );
  }
}
