import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../scoped-models/main_scope.dart';

class SignUpView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SignUpViewState();
  }
}

enum _Sex { Male, Female, PNS }

class _SignUpViewState extends State<SignUpView> {
  final Map<String, dynamic> _formData = {
    'email': null,
    'password': null,
    'birthdate': formatDate(DateTime.now(),
        [yyyy, '-', mm, '-', dd, ' ', HH, ':', nn, ':', ss, '.', SSS]),
    'country': 'Colombia',
    'height': null,
    'weight': null,
    'ethnicity': null,
    'firstName': null,
    'lastName': null,
    'sex': "I'd rather not say",
    'diseases': null,
    'doctor': false
  };
  final List<String> _dateFormat = [
    yyyy,
    '-',
    mm,
    '-',
    dd,
    ' ',
    HH,
    ':',
    nn,
    ':',
    ss,
    '.',
    SSS
  ];

  DateTime _selectedDate = DateTime.now();
  String _selectedEthnicity = 'White';
  _Sex _selectedSex = _Sex.PNS;

  bool _dateSelected = false;

  bool _selectedHD = false;
  bool _selectedHypertension = false;
  bool _selectedCOPD = false;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final ScrollController _scrollController = ScrollController();

  Map<String, TextEditingController> _controllers = {
    'email': TextEditingController(),
    'confirmEmail': TextEditingController(),
    'password': TextEditingController(),
    'confirmPassword': TextEditingController(),
    'firstName': TextEditingController(),
    'lastName': TextEditingController(),
    'height': TextEditingController(),
    'weight': TextEditingController()
  };

  Map<String, bool> _validators = {
    'email': false,
    'confirmEmail': false,
    'password': false,
    'confirmPassword': false,
    'firstName': false,
    'lastName': false,
    'height': false,
    'weight': false
  };

  final Map<String, FocusNode> _focusNodes = {
    'email': FocusNode(),
    'confirmEmail': FocusNode(),
    'password': FocusNode(),
    'confirmPassword': FocusNode(),
    'firstName': FocusNode(),
    'lastName': FocusNode(),
    'height': FocusNode(),
    'weight': FocusNode()
  };

  final FocusNode neutralFocusNode = FocusNode();

  Widget _buildEmailTextField() {
    return TextFormField(
      focusNode: _focusNodes['email'],
      maxLength: 256,
      decoration: InputDecoration(
          labelText: 'E-Mail', filled: true, fillColor: Colors.white24, counterText: ''),
      keyboardType: TextInputType.emailAddress,
      controller: _controllers['email'],
      validator: (String value) {
        if (!_emailValidator(value)) {
          return 'Please enter a valid email';
        }
      },
      onSaved: (String value) {
        _formData['email'] = value;
      },
    );
  }

  bool _emailValidator(String value) {
    _validators['email'] = false;
    if (value == null) {
      return false;
    }

    if (value.isEmpty ||
        !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]{2}(?:[a-z0-9-]*[a-z0-9])?")
            .hasMatch(value)) {
      return false;
    }
    _validators['email'] = true;
    return true;
  }

  Widget _buildEmailConfirmTextField() {
    return TextFormField(
      controller: _controllers['confirmEmail'],
      focusNode: _focusNodes['confirmEmail'],
      maxLength: 256,
      decoration: InputDecoration(
          labelText: 'Confirm E-Mail', filled: true, fillColor: Colors.white24, counterText: '',),
      keyboardType: TextInputType.emailAddress,
      validator: (String value) {
        if (!_confirmEmailValidator(value)) {
          return 'Email addresses do not match';
        }
      },
    );
  }

  bool _confirmEmailValidator(String value) {
    _validators['confirmEmail'] = false;
    if (value == null) {
      return false;
    }

    if (_controllers['email'].text != value) {
      return false;
    }

    _validators['confirmEmail'] = true;
    return true;
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      focusNode: _focusNodes['password'],
      obscureText: true,
      maxLength: 256,
      decoration: InputDecoration(
          labelText: 'Enter Password', filled: true, fillColor: Colors.white24, counterText: '',),
      controller: _controllers['password'],
      validator: (String value) {
        if (!_passwordValidator(value)) {
          return 'Password is too short.';
        }
      },
      onSaved: (String value) {
        _formData['password'] = value;
      },
    );
  }

  bool _passwordValidator(String value) {
    _validators['password'] = false;
    if (value == null) {
      return false;
    }

    if (value.isEmpty || value.length < 6) {
      return false;
    }

    _validators['password'] = true;
    return true;
  }

  Widget _buildPasswordConfirmTextField() {
    return TextFormField(
      focusNode: _focusNodes['confirmPassword'],
      maxLength: 256,
      decoration: InputDecoration(
          labelText: 'Confirm Password',
          filled: true,
          fillColor: Colors.white24,
          counterText: '',),
      obscureText: true,
      controller: _controllers['confirmPassword'],
      validator: (String value) {
        if (!_confirmPasswordValidator(value)) {
          return 'Passwords do not match.';
        }
      },
    );
  }

  bool _confirmPasswordValidator(String value) {
    _validators['confirmPassword'] = false;
    if (value == null) {
      return false;
    }

    if (_controllers['password'].text != value) {
      return false;
    }

    _validators['confirmPassword'] = true;
    return true;
  }

  Widget _buildBirthdayPicker(BuildContext context) {
    final double deviceHeight = MediaQuery.of(context).size.height;
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Column(
          children: <Widget>[
            Text(
              'Your birthdate',
              style: TextStyle(fontSize: deviceHeight * 0.021),
            ),
            SizedBox(
              height: 10.0,
            ),
            RaisedButton(
              textColor: Colors.white,
              child: _dateSelected
                  ? Text(_formData['birthdate'].substring(0, 10))
                  : Text('Select a date'),
              onPressed: () => _selectDate(context, model),
            )
          ],
        );
      },
    );
  }

  Future<Null> _selectDate(BuildContext context, MainModel model) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        firstDate: DateTime(1900),
        lastDate: DateTime.now());
    if (picked != null && picked != _formData['birthdate'])
      setState(() {
        print(formatDate(DateTime.now(), _dateFormat));
        _formData['birthdate'] = formatDate(picked, _dateFormat);
        _dateSelected = true;
        _selectedDate = picked;
      });
  }

  Widget _buildCountryPickerDropdown() {
    final double deviceHeight = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        Text(
          'Your country',
          style: TextStyle(fontSize: deviceHeight * 0.021),
        ),
        SizedBox(
          height: 20.0,
        ),
        Center(
          child: CountryPickerDropdown(
            initialValue: 'co',
            itemBuilder: _buildDropdownItem,
            onValuePicked: (Country country) {
              setState(() {
                _formData['country'] = country.name;
              });
            },
          ),
        )
      ],
    );
  }

  Widget _buildDropdownItem(Country country) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth * 0.6;
    return SizedBox(
        width: targetWidth,
        child: Text(
          "${country.name}",
        ));
  }

  Widget _buildHeightTextField() {
    return TextFormField(
      controller: _controllers['height'],
      maxLength: 5,
      focusNode: _focusNodes['height'],
      decoration: InputDecoration(
        labelText: 'Height (centimeters)',
        filled: true,
        fillColor: Colors.white24,
        counterText: '',
      ),
      keyboardType: TextInputType.number,
      validator: (String value) {
        if (!_heightValidator(value)) {
          return 'Please enter a valid height';
        }
      },
      onSaved: (String value) {
        _formData['height'] = value;
      },
    );
  }

  bool _heightValidator(String value) {
    _validators['height'] = false;
    if (value == null) {
      return false;
    }

    if (value.isEmpty || !RegExp(r"^[0-9]+$").hasMatch(value)) {
      return false;
    }

    _validators['height'] = true;
    return true;
  }

  Widget _buildWeightTextField() {
    return TextFormField(
      controller: _controllers['weight'],
      focusNode: _focusNodes['weight'],
      maxLength: 5,
      decoration: InputDecoration(
          labelText: 'Weight (kilograms)',
          filled: true,
          fillColor: Colors.white24,
          counterText: ''),
      keyboardType: TextInputType.number,
      validator: (String value) {
        if (!_weightValidator(value)) {
          return 'Please enter a valid weight';
        }
      },
      onSaved: (String value) {
        _formData['weight'] = value;
      },
    );
  }

  bool _weightValidator(String value) {
    _validators['weight'] = false;
    if (value == null) {
      return false;
    }

    if (value.isEmpty || !RegExp(r"^[0-9]+$").hasMatch(value)) {
      return false;
    }

    _validators['weight'] = true;
    return true;
  }

  Widget _buildFirstNameTextField() {
    return TextFormField(
      controller: _controllers['firstName'],
      focusNode: _focusNodes['firstName'],
      maxLength: 128,
      decoration: InputDecoration(
        labelText: 'Your name',
        filled: true,
        fillColor: Colors.white24,
        counterText: '',
      ),
      onSaved: (String value) {
        _formData['firstName'] = value;
      },
      validator: (String value) {
        if (!_firstNameValidator(value)) {
          return 'Please enter a name';
        }
      },
    );
  }

  bool _firstNameValidator(String value) {
    _validators['firstName'] = false;
    if (value == null) {
      return false;
    }

    if (value.isEmpty) {
      return false;
    }

    _validators['firstName'] = true;
    return true;
  }

  Widget _buildLastNameTextField() {
    return TextFormField(
      controller: _controllers['lastName'],
      focusNode: _focusNodes['lastName'],
      maxLength: 128,
      decoration: InputDecoration(
        labelText: 'Your last name',
        filled: true,
        fillColor: Colors.white24,
        counterText: '',
      ),
      onSaved: (String value) {
        _formData['lastName'] = value;
      },
      validator: (String value) {
        if (!_lastNameValidator(value)) {
          return 'Please enter a last name';
        }
      },
    );
  }

  bool _lastNameValidator(String value) {
    _validators['lastName'] = false;
    if (value == null) {
      return false;
    }

    if (value.isEmpty) {
      return false;
    }

    _validators['lastName'] = true;
    return true;
  }

  Widget _buildSexRadioButtons() {
    final double deviceHeight = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        Text(
          'Your gender',
          style: TextStyle(fontSize: deviceHeight * 0.021),
        ),
        SizedBox(
          height: 10.0,
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: RadioListTile<_Sex>(
                title: Text(
                  'Male',
                  style: TextStyle(fontSize: deviceHeight * 0.019),
                ),
                value: _Sex.Male,
                groupValue: _selectedSex,
                onChanged: (_Sex value) {
                  _formData['sex'] = 'Male';
                  setState(() {
                    _selectedSex = value;
                  });
                },
              ),
            ),
            Expanded(
              child: RadioListTile<_Sex>(
                title: Text(
                  'Female',
                  style: TextStyle(fontSize: deviceHeight * 0.019),
                ),
                value: _Sex.Female,
                groupValue: _selectedSex,
                onChanged: (_Sex value) {
                  print(value);
                  _formData['sex'] = 'Female';
                  setState(() {
                    _selectedSex = value;
                  });
                },
              ),
            )
          ],
        ),
        RadioListTile<_Sex>(
          title: Text(
            "I'd rather not say",
            style: TextStyle(fontSize: deviceHeight * 0.019),
          ),
          value: _Sex.PNS,
          groupValue: _selectedSex,
          onChanged: (_Sex value) {
            _formData['sex'] = "I'd rather not say";
            setState(() {
              _selectedSex = value;
            });
          },
        ),
        SizedBox(
          height: 20.0,
        )
      ],
    );
  }

  Widget _buildEthnicityDropdown() {
    final double deviceHeight = MediaQuery.of(context).size.height;
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth * 0.6;
    return Column(
      children: <Widget>[
        Text(
          'Your ethnicity',
          style: TextStyle(fontSize: deviceHeight * 0.021),
        ),
        SizedBox(
          height: 10.0,
        ),
        DropdownButton<String>(
          value: _selectedEthnicity,
          onChanged: (String newValue) {
            setState(() {
              _formData['ethnicity'] = newValue;
              _selectedEthnicity = newValue;
            });
          },
          items: <String>[
            'American Indian or Alaska Native',
            'Asian',
            'Black or African American',
            'Native Hawaiian or Other Pacific Islander',
            'White',
            'Hispanic or Latino',
            'Other'
          ].map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: SizedBox(width: targetWidth, child: Text(value)),
            );
          }).toList(),
        )
      ],
    );
  }

  Widget _buildDiseasesSwitches() {
    final double deviceHeight = MediaQuery.of(context).size.height;
    return Column(
      children: <Widget>[
        Text(
          'Your diseases',
          style: TextStyle(fontSize: deviceHeight * 0.021),
        ),
        SizedBox(
          height: 10.0,
        ),
        SwitchListTile(
          title: Text(
            'Heart disease',
            style: TextStyle(fontSize: deviceHeight * 0.019),
          ),
          value: _selectedHD,
          onChanged: (bool value) {
            setState(() {
              _selectedHD = value;
            });
          },
          secondary: const Icon(Icons.favorite_border),
        ),
        SwitchListTile(
          title: Text(
            'Hypertension',
            style: TextStyle(fontSize: deviceHeight * 0.019),
          ),
          value: _selectedHypertension,
          onChanged: (bool value) {
            setState(() {
              _selectedHypertension = value;
            });
          },
          secondary: const Icon(Icons.linear_scale),
        ),
        SwitchListTile(
          title: Text(
            'COPD',
            style: TextStyle(fontSize: deviceHeight * 0.019),
          ),
          value: _selectedCOPD,
          onChanged: (bool value) {
            setState(() {
              _selectedCOPD = value;
            });
          },
          secondary: const Icon(Icons.grain),
        )
      ],
    );
  }

  Widget _buildSignUpButton() {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return model.isLoading
            ? CircularProgressIndicator()
            : RaisedButton(
                textColor: Colors.white,
                child: Text('Sign up'),
                onPressed: () => _submitForm(model.signup),
              );
      },
    );
  }

  void _submitForm(Function signup) async {
    // Construct diseases chain

    List<String> diseases = [];

    if (_selectedCOPD) {
      diseases.add('COPD');
    }

    if (_selectedHD) {
      diseases.add('Heart disease');
    }

    if (_selectedHypertension) {
      diseases.add('Hypertension');
    }

    _formData['diseases'] = diseases;

    // Sign up process

    if (!_formKey.currentState.validate()) {
      String fieldToFocus = _validators.keys
          .firstWhere((k) => !_validators[k], orElse: () => null);

      if (fieldToFocus != null) {
        if (fieldToFocus == 'email' ||
            fieldToFocus == 'confirmEmail' ||
            fieldToFocus == 'password' ||
            fieldToFocus == 'confirmPassword') {
          _scrollController.animateTo(
            0.0,
            duration: Duration(milliseconds: 500),
            curve: Curves.easeOut,
          );
        }

        if (fieldToFocus == 'firstName' || fieldToFocus == 'lastName') {
          _scrollController.animateTo(
            200.0,
            duration: Duration(milliseconds: 500),
            curve: Curves.easeOut,
          );
        }

        if (fieldToFocus == 'height' || fieldToFocus == 'weight') {
          _scrollController.animateTo(
            800.0,
            duration: Duration(milliseconds: 500),
            curve: Curves.easeOut,
          );
        }
        await Future.delayed(Duration(milliseconds: 500));

        FocusScope.of(context).requestFocus(_focusNodes[fieldToFocus]);
      }

      return;
    }
    _formKey.currentState.save();
    final Map<String, dynamic> signupResponse = await signup(
        email: _formData['email'],
        password: _formData['password'],
        birthdate: _formData['birthdate'],
        country: _formData['country'],
        height: _formData['height'],
        weight: _formData['weight'],
        ethnicity: _formData['ethnicity'],
        firstName: _formData['firstName'],
        lastName: _formData['lastName'],
        sex: _formData['sex'],
        diseases: _formData['diseases'],
        doctor: _formData['doctor']);
    if (signupResponse['success']) {
      Navigator.pushReplacementNamed(context, '/home');
    } else {
      showDialog(
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('An error occurred.'),
              content: Text(signupResponse['message']),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
          context: context);
    }
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double deviceHeight = MediaQuery.of(context).size.height;
    final double targetWidth =
        deviceHeight < 800 ? deviceWidth * 0.8 : deviceWidth * 0.7;
    final double spaceBetween = deviceHeight * 0.01;

    return Scaffold(
        body: GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(neutralFocusNode);
      },
      child: SingleChildScrollView(
        controller: _scrollController,
        child: Center(
          child: Container(
            width: targetWidth,
            child: Form(
              key: _formKey,
              child: Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: deviceHeight * 0.1),
                    Text(
                      'Create your profile',
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: deviceHeight * 0.035,
                      ),
                    ),
                    SizedBox(height: deviceHeight * 0.03),
                    Text(
                      'You will use it later',
                      style: TextStyle(
                        color: Colors.black38,
                        fontSize: deviceHeight * 0.024,
                      ),
                    ),
                    SizedBox(height: deviceHeight * 0.03),
                    _buildEmailTextField(),
                    SizedBox(height: spaceBetween),
                    _buildEmailConfirmTextField(),
                    SizedBox(height: spaceBetween),
                    _buildPasswordTextField(),
                    SizedBox(height: spaceBetween),
                    _buildPasswordConfirmTextField(),
                    SizedBox(height: deviceHeight * 0.05),
                    Text(
                      'A little about you',
                      style: TextStyle(
                        color: Colors.black38,
                        fontSize: deviceHeight * 0.024,
                      ),
                    ),
                    SizedBox(height: deviceHeight * 0.03),
                    _buildFirstNameTextField(),
                    SizedBox(height: spaceBetween),
                    _buildLastNameTextField(),
                    SizedBox(height: spaceBetween * 3),
                    _buildSexRadioButtons(),
                    SizedBox(height: spaceBetween),
                    _buildEthnicityDropdown(),
                    SizedBox(height: deviceHeight * 0.05),
                    Text(
                      'Some extra info',
                      style: TextStyle(
                        color: Colors.black38,
                        fontSize: deviceHeight * 0.024,
                      ),
                    ),
                    SizedBox(height: deviceHeight * 0.03),
                    _buildHeightTextField(),
                    SizedBox(height: spaceBetween),
                    _buildWeightTextField(),
                    SizedBox(height: spaceBetween * 2),
                    _buildBirthdayPicker(context),
                    SizedBox(height: spaceBetween * 2),
                    _buildCountryPickerDropdown(),
                    SizedBox(height: deviceHeight * 0.05),
                    Text(
                      'Tell us about your health',
                      style: TextStyle(
                        color: Colors.black38,
                        fontSize: deviceHeight * 0.024,
                      ),
                    ),
                    SizedBox(height: deviceHeight * 0.03),
                    _buildDiseasesSwitches(),
                    SizedBox(height: spaceBetween * 4),
                    _buildSignUpButton(),
                    SizedBox(height: deviceHeight * 0.1),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    ));
  }
}
