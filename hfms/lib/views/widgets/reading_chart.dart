import 'dart:math';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class SimpleLineChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  final bool multiple;
  final double max;
  final double min;

  SimpleLineChart(this.seriesList,
      {this.animate, this.multiple, this.max, this.min});

  /// Creates a [LineChart] with sample data and no transition.
  factory SimpleLineChart.withSampleData() {
    return new SimpleLineChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: false,
      multiple: false,
    );
  }

  factory SimpleLineChart.withData(List<double> values, var color) {
    double max = 0;
    double min = 100;

    for (var i = 0; i < values.length; i++) {
      if (values[i] > max) {
        max = values[i];
      }
      if (values[i] < min) {
        min = values[i];
      }
    }

    double space = (max - min) * 0.05;

    return new SimpleLineChart(
      _createData(values, color),
      // Disable animations for image tests.
      animate: false,
      multiple: false,
      max: max + space,
      min: min - space,
    );
  }

  factory SimpleLineChart.withMultipleData(
      List<List<double>> listValues, List<String> names, List<dynamic> colors) {
    double max = 0;
    double min = 100;

    for (var i = 0; i < listValues.length; i++) {
      for (var j = 0; j < listValues[i].length; j++) {
        if (listValues[i][j] > max) {
          max = listValues[i][j];
        }
        if (listValues[i][j] < min) {
          min = listValues[i][j];
        }
      }
    }

    double space = (max - min) * 0.05;

    return new SimpleLineChart(
      _createMultipleData(listValues, names, colors),
      // Disable animations for image tests.
      animate: false,
      multiple: true,
      max: max + space,
      min: min - space,
    );
  }

  // EXCLUDE_FROM_GALLERY_DOCS_START
  // This section is excluded from being copied to the gallery.
  // It is used for creating random series data to demonstrate animation in
  // the example app only.
  factory SimpleLineChart.withRandomData() {
    return new SimpleLineChart(_createRandomData());
  }

  /// Create random data.
  static List<charts.Series<LinearSales, num>> _createRandomData() {
    List colors = [
      charts.MaterialPalette.blue.shadeDefault,
      charts.MaterialPalette.cyan.shadeDefault,
      charts.MaterialPalette.purple.shadeDefault,
      charts.MaterialPalette.red.shadeDefault,
      charts.MaterialPalette.deepOrange.shadeDefault,
      charts.MaterialPalette.green.shadeDefault
    ];

    var color = colors[Random().nextInt(5)];

    final random = new Random();

    final data = [
      new LinearSales(0, random.nextInt(100).toDouble()),
      new LinearSales(1, random.nextInt(100).toDouble()),
      new LinearSales(2, random.nextInt(100).toDouble()),
      new LinearSales(3, random.nextInt(100).toDouble()),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: 'Sales',
        colorFn: (_, __) => color,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }
  // EXCLUDE_FROM_GALLERY_DOCS_END

  @override
  Widget build(BuildContext context) {
    if (multiple) {
      return new charts.LineChart(
        seriesList,
        animate: animate,
        // Add the legend behavior to the chart to turn on legends.
        // By default the legend will display above the chart.
        //
        // To change the symbol used in the legend, set the renderer attribute of
        // symbolRendererKey to a SymbolRenderer.

        behaviors: [
          new charts.SeriesLegend(
              position: charts.BehaviorPosition.end,
              insideJustification: charts.InsideJustification.topEnd,
              outsideJustification: charts.OutsideJustification.middle,
              showMeasures: true)
        ],
        primaryMeasureAxis: new charts.NumericAxisSpec(
          tickProviderSpec: new charts.StaticNumericTickProviderSpec(
            <charts.TickSpec<num>>[
              charts.TickSpec<num>(min),
              charts.TickSpec<num>(max),
            ],
          ),
        ),
      );
    } else {
      return new charts.LineChart(
        seriesList,
        animate: animate,
        primaryMeasureAxis: new charts.NumericAxisSpec(
          tickProviderSpec: new charts.StaticNumericTickProviderSpec(
            <charts.TickSpec<num>>[
              charts.TickSpec<num>(min),
              charts.TickSpec<num>(max),
            ],
          ),
        ),
      );
    }
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<LinearSales, int>> _createSampleData() {
    List colors = [
      charts.MaterialPalette.yellow.shadeDefault,
      charts.MaterialPalette.blue.shadeDefault,
      charts.MaterialPalette.cyan.shadeDefault,
      charts.MaterialPalette.purple.shadeDefault,
      charts.MaterialPalette.red.shadeDefault,
      charts.MaterialPalette.green.shadeDefault,
      charts.MaterialPalette.gray.shadeDefault,
      charts.MaterialPalette.indigo.shadeDefault,
      charts.MaterialPalette.lime.shadeDefault,
      charts.MaterialPalette.pink.shadeDefault,
      charts.MaterialPalette.teal.shadeDefault,
      charts.MaterialPalette.deepOrange.shadeDefault,
      charts.MaterialPalette.black
    ];

    final data = [
      new LinearSales(0, 5),
      new LinearSales(1, 25),
      new LinearSales(2, 100),
      new LinearSales(3, 75),
    ];

    var color = colors[Random().nextInt(colors.length - 1)];

    return [
      new charts.Series<LinearSales, int>(
        id: 'Sales',
        colorFn: (_, __) => color,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }

  static List<charts.Series<LinearSales, int>> _createData(
      List<double> values, var color) {
    final List<LinearSales> data = [];

    for (var i = 0; i < values.length; i++) {
      data.add(new LinearSales(i, values[i]));
    }

    return [
      new charts.Series<LinearSales, int>(
        id: 'Sales',
        colorFn: (_, __) => color,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }

  static List<charts.Series<LinearSales, int>> _createMultipleData(
      List<List<double>> listValues, List<String> names, List<dynamic> colors) {
    final List<charts.Series<LinearSales, int>> multipleData = [];

    for (var j = 0; j < listValues.length; j++) {
      List<LinearSales> data = [];
      List<double> values = listValues[j];

      for (var i = 0; i < values.length; i++) {
        data.add(new LinearSales(i, values[i]));
      }

      var color = colors[j];

      multipleData.add(new charts.Series<LinearSales, int>(
        id: names[j],
        colorFn: (_, __) => color,
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data,
      ));
    }

    return multipleData;
  }
}

/// Sample linear data type.
class LinearSales {
  final int year;
  final double sales;

  LinearSales(this.year, this.sales);
}