import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import './reading_chart.dart';
import '../../models/reading.dart';
import '../../scoped-models/main_scope.dart';
import '../user/reading.dart';

class ReadingCard extends StatelessWidget {
  final Reading reading;
  final int readingIndex;

  ReadingCard(this.reading, this.readingIndex);

  Widget _buildReadingCard() {
    Widget _buildChart(BuildContext context) {
      final double deviceHeight = MediaQuery.of(context).size.height;
      final double targetHeight = deviceHeight * 0.15;
      final double deviceWidth = MediaQuery.of(context).size.width;
      final double targetWidth = deviceWidth * 0.70;
      List<double> cardHR = List.from(reading.heartRate);

      return Container(
          height: targetHeight,
          width: targetWidth,
          child: SimpleLineChart.withData(cardHR, reading.color));
    }

    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        final double deviceHeight = MediaQuery.of(context).size.height;
        final double targetHeight =
            deviceHeight < 800 ? deviceHeight * 0.31 : deviceHeight * 0.29;

        if (reading.processed) {
          return SizedBox(
            height: targetHeight,
            child: Card(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: deviceHeight * 0.015,
                  ),
                  Text(
                      reading.locale != null
                          ? '${reading.locale}'
                          : 'Unavailable',
                      style: TextStyle(fontSize: deviceHeight * 0.02)),
                  SizedBox(
                    height: deviceHeight * 0.005,
                  ),
                  Text(
                      reading.timestamp.substring(0, 10) +
                          " " +
                          reading.timestamp.substring(11, 16),
                      style: TextStyle(
                          fontSize: deviceHeight * 0.015,
                          color: Colors.black38)),
                  _buildChart(context),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: IconButton(
                          icon: Icon(Icons.share, color: Theme.of(context).buttonColor,),
                          tooltip: 'Share reading',
                          onPressed: () {
                            Share.text('My reading!', reading.textToShare(),
                                'text/plain');
                          },
                          iconSize: deviceHeight * 0.025,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: IconButton(
                          icon: Icon(Icons.info_outline, color: Theme.of(context).buttonColor,),
                          tooltip: 'More info',
                          onPressed: () {
                            model.selectReading(readingIndex);
                            model.fetchComments();
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (BuildContext context) {
                                  return ReadingView(
                                      model.readings[readingIndex],
                                      model.readings[readingIndex].color);
                                },
                              ),
                            ).then((_) => {model.cancelCommentsSuscriptions()});
                          },
                          iconSize: deviceHeight * 0.025,
                        ),
                      )),
                    ],
                  )
                ],
              ),
            ),
          );
        } else {
          return SizedBox(
            height: targetHeight,
            child: Card(
              child: Center(
                child: Text(
                  'Processing...',
                  style: TextStyle(
                      fontSize: deviceHeight * 0.03,
                      fontStyle: FontStyle.italic,
                      color: Colors.grey.shade600),
                ),
              ),
            ),
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildReadingCard();
  }
}
