import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import './patient.dart';
import '../../scoped-models/main_scope.dart';

class PatientsTab extends StatelessWidget {
  Widget _buildPatientsList(BuildContext context, MainModel model) {
    Widget patientsList = Text(
      "You do not have patients yet",
      textAlign: TextAlign.center,
    );

    List<Widget> patients = [];

    for (var i = 0; i < model.relationsList.length; i++) {
      patients.add(ListTile(
        leading: const Icon(Icons.person),
        key: Key(model.relationsList[i].id.toString()),
        title: Text(model.relationsList[i].firstName +
            ' ' +
            model.relationsList[i].lastName),
        subtitle: Text(model.relationsList[i].email),
        onTap: () {
          model.setLoading(true);
          model.selectRelation(i).then((_) {
            model.fetchReadings(model.relationsList[i].id).then((_) {
              model.setLoading(false);
            });
          });
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) {
                return PatientView();
              },
            ),
          ).then((_) {
            model.cancelReportsSuscriptions();
          });
        },
      ));
    }

    if (model.relationsList.length > 0) {
      patientsList = Column(
        children: patients,
      );
    }

    return patientsList;
  }

  Widget _buildPatientsRequestsList(BuildContext context, MainModel model) {
    Widget patientsRequestsList = Text(
      "You do not have requests yet",
      textAlign: TextAlign.center,
    );

    final double deviceWidth = MediaQuery.of(context).size.width;

    List<Widget> requests = [];

    for (var i = 0; i < model.requestsList.length; i++) {
      requests.add(ListTile(
        leading: CircleAvatar(
          backgroundImage: NetworkImage(
              'https://cdn-img.instyle.com/sites/default/files/styles/684xflex/public/1536155340/090518-liam-hemsworth-tout.jpg?itok=bTw3xvsY'),
        ),
        key: Key(model.requestsList[i].id.toString()),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    model.requestsList[i].firstName +
                        ' ' +
                        model.requestsList[i].lastName,
                    style: TextStyle(fontSize: deviceWidth * 0.04),
                  ),
                  Text(
                    model.requestsList[i].email,
                    style: TextStyle(
                        fontSize: deviceWidth * 0.035,
                        color: Colors.grey.shade500),
                  )
                ],
              ),
            ),
            GestureDetector(
              child: Icon(
                Icons.close,
                color: Colors.red.shade800,
                size: deviceWidth * 0.07,
              ),
              onTap: () {
                model.rejectRelation(model.requestsList[i]);
              },
            ),
            SizedBox(
              width: deviceWidth * 0.05,
            ),
            GestureDetector(
              child: Icon(
                Icons.check,
                color: Colors.green.shade700,
                size: deviceWidth * 0.07,
              ),
              onTap: () {
                model.acceptRelation(model.requestsList[i]);
              },
            ),
          ],
        ),
      ));
    }

    if (model.requestsList.length > 0) {
      patientsRequestsList = Column(
        children: requests,
      );
    }

    return patientsRequestsList;
  }

  Widget _buildTab() {
    return ScopedModelDescendant<MainModel>(
        builder: (BuildContext context, Widget child, MainModel model) {
      return Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 30.0),
              Text(
                'Your patients',
                style: TextStyle(
                    fontSize: 25.0, color: Theme.of(context).primaryColor),
              ),
              SizedBox(height: 15.0),
              _buildPatientsList(context, model),
              SizedBox(height: 30.0),
              Text(
                'Your patient requests',
                style: TextStyle(
                    fontSize: 25.0, color: Theme.of(context).primaryColor),
              ),
              SizedBox(height: 15.0),
              _buildPatientsRequestsList(context, model)
            ],
          ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(child: _buildTab()),
    );
  }
}
