import 'package:flutter/material.dart';
import '../../scoped-models/main_scope.dart';
import 'package:scoped_model/scoped_model.dart';
import '../widgets/reading_card.dart';

class PatientView extends StatelessWidget {
  final ScrollController _scrollController = ScrollController();

  List<Widget> _buildDiseasesChips(BuildContext context, MainModel model) {
    List<Widget> chips = [];

    model.relationUser.diseases.forEach((element) {
      chips.add(Container(
        child: Chip(
          backgroundColor: Colors.grey.shade400,
          label: Text(
            element,
            style: TextStyle(color: Colors.white),
          ),
        ),
        padding: EdgeInsets.all(2.0),
      ));
    });

    return chips;
  }

  Widget _buildReadingsList(context, model) {
    Widget readingCards;
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 50.0 : deviceWidth * 0.1;
    readingCards = Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(60.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Your patient does not have reports yet',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              'Once they create one, you will be able to view it here',
              textAlign: TextAlign.center,
            )
          ],
        ));
    if (model.readings.length > 0 && !model.isLoading) {
      // model.sortReadings();
      readingCards = ListView.builder(
        physics: AlwaysScrollableScrollPhysics(),
        controller: _scrollController,
        padding: EdgeInsets.all(targetWidth),
        reverse: false,
        itemBuilder: (BuildContext context, int index) {
          Widget card = ReadingCard(model.readings[index], index);
          return card;
        },
        itemCount: model.readings.length,
      );
    } else if (model.isLoading) {
      readingCards = Center(child: CircularProgressIndicator());
    }
    return readingCards;
  }

  Widget _buildProfileList(BuildContext context, MainModel model) {
    return ListView(
      padding: const EdgeInsets.all(30.0),
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              model.relationUser.firstName + ' ' + model.relationUser.lastName,
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 25.0,
              ),
            ),
            /* CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://cdn-img.instyle.com/sites/default/files/styles/684xflex/public/1536155340/090518-liam-hemsworth-tout.jpg?itok=bTw3xvsY'),
                  radius: deviceWidth * 0.3,
                ), */
            SizedBox(
              height: 20,
            ),
            ListTile(
              leading: const Icon(Icons.account_circle),
              title: Text(model.relationUser.firstName +
                  " " +
                  model.relationUser.lastName),
              subtitle: const Text('Name'),
            ),
            ListTile(
              leading: const Icon(Icons.email),
              title: Text(model.relationUser.email),
              subtitle: const Text('Email'),
            ),
            ListTile(
              leading: const Icon(Icons.date_range),
              title: Text(model.relationUser.birthdate.substring(0, 10)),
              subtitle: const Text('Birthdate'),
            ),
            ListTile(
              leading: const Icon(Icons.place),
              title: Text(model.relationUser.country),
              subtitle: const Text('Country'),
            ),
            ListTile(
              leading: const Icon(Icons.wc),
              title: Text(model.relationUser.sex),
              subtitle: const Text('Sex'),
            ),
            ListTile(
              leading: const Icon(Icons.nature_people),
              title: Text(model.relationUser.height),
              subtitle: const Text('Height'),
            ),
            ListTile(
              leading: const Icon(Icons.fitness_center),
              title: Text(model.relationUser.weight),
              subtitle: const Text('Weight'),
            ),
            SizedBox(
              height: 10.0,
            ),
            Divider(
              height: 1.0,
            ),
            SizedBox(
              height: 15.0,
            ),
            Container(
              child: Text(
                'Diseases',
                style: TextStyle(fontSize: 20.0),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Wrap(
              children: _buildDiseasesChips(context, model),
            ),
            SizedBox(
              height: 15.0,
            )
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    //buildChartProgressively(reading.plethismogram);
    return WillPopScope(
      onWillPop: () {
        return Future(() => true);
      },
      child: _buildTabs());
  }

  Widget _buildTabs() {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Patient'),
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(Icons.account_circle)),
              Tab(icon: Icon(Icons.event_note))
            ],
          ),
        ),
        body: TabBarView(
          children: [
            _buildProfileBarView(),
            _buildReadingsBarView(),
          ],
        ),
      ),
    );
  }

  Widget _buildProfileBarView() {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return model.isLoading ? Center(child: CircularProgressIndicator()) : _buildProfileList(context, model);
      },
    );
  }

  Widget _buildReadingsBarView() {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return model.isLoading ? Center(child: CircularProgressIndicator()) : _buildReadingsList(context, model);
      },
    );
  }
}
