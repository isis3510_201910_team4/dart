import 'package:flutter/material.dart';

import './patients.dart';
import './profile.dart';
import '../../scoped-models/main_scope.dart';

class BrowserViewDoctor extends StatefulWidget {
  final MainModel model;
  final int selectedIndex;
  BrowserViewDoctor(this.model, {this.selectedIndex = 0});

  @override
  State<StatefulWidget> createState() {
    return _BrowserViewState(selectedIndex);
  }
}

class _BrowserViewState extends State<BrowserViewDoctor> {
  int selectedIndex;
  _BrowserViewState(this.selectedIndex);

  @override
  initState() {
    widget.model.fetchRelations();
    super.initState();
  }

  final _widgetOptions = [
    PatientsTab(),
    ProfileTab(),
  ];

  Widget _buildBottomNavigationBar() {
    return BottomNavigationBar(
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.healing),
          title: Text('Patients'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          title: Text('Profile'),
        ),
      ],
      currentIndex: selectedIndex,
      onTap: _onItemTapped,
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        elevation: 2.0,
        title: Text(
          'HeartU',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Center(
        child: _widgetOptions.elementAt(selectedIndex),
      ),
      bottomNavigationBar: _buildBottomNavigationBar(),
    );
  }
}
