import 'package:flutter/material.dart';
import '../../scoped-models/main_scope.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileTab extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _ProfileTabState();
  }
}

class _ProfileTabState extends State<ProfileTab> {

  bool fingerPrintPreference = MySharedPreferences._myBool;

  @override
  void initState() {
    super.initState();
    
    MySharedPreferences.load();
  }

  Widget _buildProfileList(BuildContext context) {    

    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return ListView(
          padding: const EdgeInsets.all(30.0),
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  'Profile',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 25.0,
                  ),
                ),
                /* CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://cdn-img.instyle.com/sites/default/files/styles/684xflex/public/1536155340/090518-liam-hemsworth-tout.jpg?itok=bTw3xvsY'),
                  radius: deviceWidth * 0.3,
                ), */
                SizedBox(
                  height: 20,
                ),
                ListTile(
                  leading: const Icon(Icons.account_circle),
                  title: Text(model.authenticatedUser.firstName +
                      " " +
                      model.authenticatedUser.lastName),
                  subtitle: const Text('Name'),
                ),
                ListTile(
                  leading: const Icon(Icons.email),
                  title: Text(model.authenticatedUser.email),
                  subtitle: const Text('Email'),
                ),
                ListTile(
                  leading: const Icon(Icons.date_range),
                  title:
                      Text(model.authenticatedUser.birthdate.substring(0, 10)),
                  subtitle: const Text('Birthdate'),
                ),
                ListTile(
                  leading: const Icon(Icons.place),
                  title: Text(model.authenticatedUser.country),
                  subtitle: const Text('Country'),
                ),
                ListTile(
                  leading: const Icon(Icons.wc),
                  title: Text(model.authenticatedUser.sex),
                  subtitle: const Text('Sex'),
                ),
                ListTile(
                  leading: const Icon(Icons.nature_people),
                  title: Text(model.authenticatedUser.height),
                  subtitle: const Text('Height'),
                ),
                ListTile(
                  leading: const Icon(Icons.fitness_center),
                  title: Text(model.authenticatedUser.weight),
                  subtitle: const Text('Weight'),
                ),
                ListTile(
                  leading: const Icon(Icons.fitness_center),
                  title: Text(model.relationsList.length.toString()),
                  subtitle: const Text('Patients'),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Divider(
                  height: 1.0,
                ),
                SizedBox(
                  height: 15.0,
                ),
                Container(
                  child: Text(
                    'Account',
                    style: TextStyle(fontSize: 20.0),
                  ),
                ),
                /* SizedBox(
                  height: 15.0,
                ),
                SwitchListTile(
                  title: Text(
                    'Enable fingerprint log',
                    style: TextStyle(fontSize: 15.0),
                  ),
                  value: MySharedPreferences._ready ? MySharedPreferences._myBool : model.fingerprintLog,
                  onChanged: (bool value) {
                    _changeFingerPrintPreference(value);
                  },
                  secondary: const Icon(Icons.fingerprint),
                ), */
                SizedBox(
                  height: 15.0,
                ),
                RaisedButton(
                  color: Theme.of(context).primaryColor,
                  child: Text(
                    'Logout',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    model.logout();
                    Navigator.of(context).pushReplacementNamed('/');
                  },
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth * 0.9;

    return Container(
      child: _buildProfileList(context),
    );
  }
}

class MySharedPreferences {
  static bool _myBool = false;
  static bool _ready = false;

  static void load() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _myBool = prefs.getBool('fingerprintLog') ?? false;
    _ready = true;
  }

  static void save(bool myBool) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _myBool = myBool;
    await prefs.setBool('fingerprintLog', _myBool);
  }

  static void printMyBool() {
    print('fingerprintLog: ${_myBool.toString()}');
  }
}
