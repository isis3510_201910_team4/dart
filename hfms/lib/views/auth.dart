import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../scoped-models/main_scope.dart';

class AuthView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthViewState();
  }
}

class _AuthViewState extends State<AuthView> {
  final Map<String, String> _formData = {
    'username': null,
    'password': null,
  };

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildHeartIcon() {
    final double deviceHeigth = MediaQuery.of(context).size.height;
    return SizedBox(
      child: Image.asset('assets/heartURed.png'),
      height: deviceHeigth * 0.14,
      width: deviceHeigth * 0.14,
    );
  }

  Widget _buildHeader() {
    return Text(
      'HeartU',
      style: TextStyle(
        color: Theme.of(context).primaryColor,
        fontSize: 30.0,
      ),
    );
  }

  Widget _buildUsernameTextField() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Username',
        filled: true,
        fillColor: Colors.white24,
      ),
      keyboardType: TextInputType.emailAddress,
      onSaved: (String value) {
        _formData['username'] = value;
      },
      validator: (String value) {
        if (value.isEmpty) {
          return 'Please enter an email address';
        }
        if (!RegExp(
                r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]{2}(?:[a-z0-9-]*[a-z0-9])?")
            .hasMatch(value)) {
          return 'Please enter a valid email address';
        }
      },
    );
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Password', filled: true, fillColor: Colors.white24),
      obscureText: true,
      onSaved: (String value) {
        _formData['password'] = value;
      },
      validator: (String value) {
        if (value.isEmpty) {
          return 'Please enter a password';
        }
        if (value.length < 6) {
          return 'Passwords must be over 5 characters long';
        }
      },
    );
  }

  Widget _buildLoginButton(targetWidth) {
    return ScopedModelDescendant<MainModel>(
      builder: (BuildContext context, Widget child, MainModel model) {
        return model.isLoading
            ? CircularProgressIndicator()
            : SizedBox(
                width: targetWidth,
                child: RaisedButton(
                  elevation: 5.0,
                  textColor: Colors.white,
                  child: Text('LOGIN'),
                  onPressed: () => _submitForm(model.authenticate),
                ),
              );
      },
    );
  }

  void _submitForm(Function authenticate) async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    final Map<String, dynamic> successInformation =
        await authenticate(_formData['username'], _formData['password']);
    if (successInformation['success']) {
      Navigator.pushReplacementNamed(context, '/home');
    } else {
      showDialog(
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(successInformation['title']),
              content: Text(successInformation['message']),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
          context: context);
    }
  }

  Widget _buildSignUpButton() {
    return FlatButton(
      child: Text('Sign Up to HeartU'),
      onPressed: () => Navigator.pushNamed(context, '/signup'),
    );
  }

  @override
  Widget build(BuildContext context) {
    final double deviceWidth = MediaQuery.of(context).size.width;
    final double targetWidth = deviceWidth > 550.0 ? 500.0 : deviceWidth * 0.7;
    return Scaffold(
        backgroundColor: Colors.white,
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Container(
            child: Center(
              child: SingleChildScrollView(
                child: Container(
                  width: targetWidth,
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        _buildHeartIcon(),
                        SizedBox(height: 30),
                        _buildHeader(),
                        SizedBox(height: 40),
                        _buildUsernameTextField(),
                        SizedBox(height: 20),
                        _buildPasswordTextField(),
                        SizedBox(height: 40),
                        _buildLoginButton(targetWidth),
                        _buildSignUpButton()
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
