import 'dart:async';
import 'package:flutter/services.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import './scoped-models/main_scope.dart';
import './views/auth.dart';
import './views/user/browser.dart';
import './views/doctor/browser.dart';
import './views/signup.dart';

void main() async {
  final FirebaseApp app = await FirebaseApp.configure(
    name: 'db2',
    options: const FirebaseOptions(
            googleAppID: '1:341141793511:android:4fc5ab3115e6e35a',
            apiKey: 'AIzaSyDPHo6qdZT5Y3NjmwSzEBhfaBmq-DDGVbM',
            databaseURL: 'https://heartu-ba7e0.firebaseio.com',
          ),
  );
  
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  runApp(MyApp(app: app));
}

class MyApp extends StatefulWidget {
  final FirebaseApp app;
  MyApp({this.app});
  
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final MainModel _model = MainModel();
  bool _isAuthenticated = false;

  @override
  void initState() {
    _model.setFirebaseApp(widget.app);
    _model.autoAuthenticate();
    _model.userSubject.listen((bool isAuthenticated) {
      setState(() {
        _isAuthenticated = isAuthenticated;
      });
    });
    super.initState();
  }

  @override
  void dispose(){
    super.dispose();
    _model.cancelSubscriptions();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel<MainModel>(
      model: _model,
      child: MaterialApp(
        theme: ThemeData(
          primaryColor: Color(0xFFE85257),
          buttonColor: Color(0xFFE85257),
          accentColor: Color(0xFFE85257),
          // highlightColor: Color(0xFFFF715B),
        ),
        routes: {
          '/': (BuildContext context) =>
              !_isAuthenticated ? AuthView() : _model.authenticatedUser.doctor ? BrowserViewDoctor(_model) : BrowserViewUser(_model),
          '/home': (BuildContext context) => _model.authenticatedUser.doctor ? BrowserViewDoctor(_model) : BrowserViewUser(_model),
          '/signup': (BuildContext context) => SignUpView(),
        },
      ),
    );
  }
}
