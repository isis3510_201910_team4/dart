import 'package:flutter/material.dart';

class Comment {
  final String id;
  final String readingId;
  final String userId;
  final String content;
  final bool doctor;
  final String userName;
  final String timestamp;

  Comment(
      {@required this.id,
      @required this.readingId,
      @required this.userId,
      @required this.content,
      @required this.doctor,
      @required this.userName,      
      @required this.timestamp});
}
