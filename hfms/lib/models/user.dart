import 'package:flutter/material.dart';

class User {
  final String id;
  final String token;
  final String birthdate;
  final String country;
  final List<dynamic> diseases;
  final String email;
  final String ethnicity;
  final String firstName;
  final String height;
  final String lastName;
  final String sex;
  final String weight;
  final bool doctor;

  User({
    @required this.id,
    @required this.token,
    @required this.birthdate,
    @required this.country,
    @required this.diseases,
    @required this.email,
    @required this.ethnicity,
    @required this.firstName,
    @required this.height,
    @required this.lastName,
    @required this.sex,
    @required this.weight,
    @required this.doctor
  });
}
