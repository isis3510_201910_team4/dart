import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

class Reading {
  final String id;
  final double averageHeartRate;
  final List<double> heartRate;
  final String timestamp;
  final dynamic color;
  final String locale;
  final double lat;
  final double lon;
  final String activity;
  final String moreInfo;
  final bool processed;

  Reading(
      {@required this.id,
      this.averageHeartRate,
      this.heartRate,
      @required this.timestamp,
      @required this.color,
      @required this.locale,
      @required this.lat,
      @required this.lon,
      @required this.activity,
      this.moreInfo,
      @required this.processed});

  final List<String> shareDateFormat = [
    DD,
    ' ',
    dd,
    ' of ',
    MM,
    ', ',
    yyyy,
    ' at ',
    hh,
    ':',
    nn,
    am
  ];
  final List<String> dateFormat = [
    yyyy,
    '-',
    mm,
    '-',
    dd,
    ' ',
    HH,
    ':',
    nn,
    ':',
    ss,
    '.',
    SSS
  ];

  String textToShare() {
    String textToShare;

    int year = int.parse(timestamp.substring(0, 4));
    int month = int.parse(timestamp.substring(5, 7));
    int day = int.parse(timestamp.substring(8, 10));
    int hour = int.parse(timestamp.substring(11, 13));
    int minute = int.parse(timestamp.substring(14, 16));
    DateTime unformattedDateTime = DateTime(year, month, day, hour, minute);
    String formattedDate = formatDate(unformattedDateTime, shareDateFormat);

    textToShare = 'This is my reading from $formattedDate';
    textToShare += locale != null ? ', taken in $locale:\n' : ':\n';
    textToShare += 'Average Heart Rate: $averageHeartRate\n';
    textToShare += 'Activity: $activity\n';
    if (moreInfo.length > 0) {
      textToShare += 'More info: $moreInfo\n';
    }
    return textToShare;
  }
}
