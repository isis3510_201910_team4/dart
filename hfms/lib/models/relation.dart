import 'package:flutter/material.dart';

class Relation {
  final String id;
  final String email;
  final String firstName;
  final String lastName;

  Relation(
      {@required this.id,
      @required this.email,
      @required this.firstName,
      @required this.lastName});
}
