import 'package:date_format/date_format.dart';
import 'package:firebase_database/firebase_database.dart';

import './connected_scope.dart';
import '../models/comment.dart';

mixin CommentsModel on ConnectedScope {

  Future<Null> fetchComments() {
    // Set loading variable for Spinners
    isLoading = true;
    notifyListeners();

    DatabaseReference commentsForReadingRef =
        commentsRef.child(selectedReading.id).reference();
    commentsList = [];
    commentsSubscription
        .add(commentsForReadingRef.onChildAdded.listen((Event event) {
      dynamic commentData = event.snapshot.value;

      final Comment comment = Comment(
          id: event.snapshot.key,
          content: commentData['content'],
          doctor: commentData['doctor'],
          userId: commentData['userId'],
          readingId: selectedReading.id,
          userName: commentData['userName'],
          timestamp: commentData['timestamp']);

      int index = commentsList.indexWhere((item) => item.id == comment.id);
      int sortIndex = readingsList
          .lastIndexWhere((item) => item.id.compareTo(comment.id) > 0);

      if (index == -1) {
        commentsList.insert(sortIndex + 1, comment);
      }

      notifyListeners();
    }));

    commentsSubscription
        .add(commentsForReadingRef.onChildRemoved.listen((Event event) {
      commentsList.removeWhere((item) => item.id == event.snapshot.key);
      notifyListeners();
    }));

    isLoading = false;
    return null;
  }

  Future<void> addComment(String content) async {
    // Set loading variable for Spinners
    isLoading = true;
    notifyListeners();

    // Create the new node in the database
    DatabaseReference commentsForReadingRef =
        commentsRef.child(selectedReading.id);

    ///////////////////

    Map<String, dynamic> commentData = {
      'content': content,
      'doctor': authenticatedUser.doctor,
      'readingId': selectedReading.id,
      'userName':
          '${authenticatedUser.firstName} ${authenticatedUser.lastName}',
      'timestamp': formatDate(DateTime.now(), dateFormat),
      'userId': authenticatedUser.id
    };

    ///////////////////

    DatabaseReference pushedCommentRef = commentsForReadingRef.push();
    String newCommentId = pushedCommentRef.key;

    ///////////////////
    final Comment newComment = Comment(
        id: newCommentId,
        content: commentData['content'],
        doctor: commentData['doctor'],
        readingId: selectedReading.id,
        userId: authenticatedUser.id,
        userName: commentData['userName'],
        timestamp: commentData['timestamp']);

    commentsList.insert(0, newComment);

    // Set loading variable for Spinners
    isLoading = false;
    notifyListeners();

    return pushedCommentRef.set(commentData);
  }
}
