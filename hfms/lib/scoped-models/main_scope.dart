import 'package:scoped_model/scoped_model.dart';
import './readings.dart';
import './users.dart';
import './connected_scope.dart';
import './utility.dart';
import './device.dart';
import './relations.dart';
import './comments.dart';

class MainModel extends Model with ConnectedScope, UsersModel, ReadingsModel, UtilityModel, DeviceModel, RelationsModel, CommentsModel {
}