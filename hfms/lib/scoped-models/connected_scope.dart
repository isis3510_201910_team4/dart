import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:scoped_model/scoped_model.dart';

import '../models/reading.dart';
import '../models/user.dart';
import '../models/relation.dart';
import '../models/comment.dart';
import 'package:date_format/date_format.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:connectivity/connectivity.dart';

mixin ConnectedScope on Model {
  final List<String> dateFormat = [
    yyyy,
    '-',
    mm,
    '-',
    dd,
    ' ',
    HH,
    ':',
    nn,
    ':',
    ss,
    '.',
    SSS
  ];

  List<Reading> readingsList = [];
  List<Relation> relationsList = [];
  List<Relation> requestsList = [];
  List<Reading> readingsSelectedPatient = [];
  List<Comment> commentsList = [];

  double sendingVideoPercent = 0;
  StorageUploadTask videoUploading;
  bool videoUploadingCanceled = false;

  User authenticatedUser;
  User relationUser;
  int selectedReadingIndex;
  int selectedRelationIndex;
  bool isLoading = false;
  Reading get selectedReading => readingsList[selectedReadingIndex];
  Relation get selectedRelation => relationsList[selectedRelationIndex];
  User get user => authenticatedUser;

  Map<String, double> avgHRByActivity = {
    'resting': 0,
    'exercise': 0,
    'alcohol': 0,
    'woke': 0
  };

  Map<String, int> numReportsByActivity = {
    'resting': 0,
    'exercise': 0,
    'alcohol': 0,
    'woke': 0
  };

  FirebaseApp app;
  DatabaseReference reportsRef;
  DatabaseReference usersRef;
  DatabaseReference relationsRef;
  DatabaseReference commentsRef;
  StorageReference videosRef;

  List<StreamSubscription<Event>> reportsSubscription = [];
  List<StreamSubscription<Event>> usersSubscription = [];
  List<StreamSubscription<Event>> relationsSubscription = [];
  List<StreamSubscription<Event>> commentsSubscription = [];
  // StreamSubscription<connectivity.ConnectivityResult> connectivitySubscription;

  Future<ConnectivityResult> checkConnectivityApp() async {
    return (Connectivity().checkConnectivity());   
  }

  void setFirebaseApp(FirebaseApp appToSet) {
    app = appToSet;
    reportsRef = FirebaseDatabase.instance.reference().child('reports');
    usersRef = FirebaseDatabase.instance.reference().child('users');
    relationsRef = FirebaseDatabase.instance.reference().child('relations');
    videosRef = FirebaseStorage.instance.ref().child('videos/');
    commentsRef = FirebaseDatabase.instance.reference().child('comments');

    reportsRef.keepSynced(true);
    usersRef.keepSynced(true);
    relationsRef.keepSynced(true);
    commentsRef.keepSynced(true);
  }

  void cancelReportsSuscriptions() {
    reportsSubscription.forEach((s) {
      s.cancel();
    });
    reportsSubscription = [];
    readingsList = [];
  }

  void cancelUsersSuscriptions() {
    usersSubscription.forEach((s) {
      s.cancel();
    });
    usersSubscription = [];
  }

  void cancelRelationsSuscriptions() {
    relationsSubscription.forEach((s) {
      s.cancel();
    });
    relationsSubscription = [];
    relationsList = [];
    requestsList = [];
  }

  void cancelCommentsSuscriptions() {
    commentsSubscription.forEach((s) {
      s.cancel();
    });
    commentsSubscription = [];
    commentsList = [];
  }

  void cancelSubscriptions() {
    cancelReportsSuscriptions();
    cancelUsersSuscriptions();
    cancelRelationsSuscriptions();
    cancelCommentsSuscriptions();
    // connectivitySubscription.cancel();
  }

  void setPercentSendingVideo(double percentage) {
    sendingVideoPercent = percentage;
    notifyListeners();
  }

  void setLoading(bool load) {
    isLoading = load;
    notifyListeners();
  }

  void cancelSending() {
    if (videoUploading != null) {
      videoUploading.cancel();
    }
    videoUploading = null;
    videoUploadingCanceled = true;
  }
}
