import 'package:firebase_database/firebase_database.dart';
import './connected_scope.dart';
import '../models/reading.dart';
import 'dart:async';
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:math';
import 'dart:isolate';

mixin ReadingsModel on ConnectedScope {
  final List colors = [
    charts.MaterialPalette.blue.shadeDefault,
    charts.MaterialPalette.cyan.shadeDefault,
    charts.MaterialPalette.purple.shadeDefault,
    charts.MaterialPalette.red.shadeDefault,
    charts.MaterialPalette.deepOrange.shadeDefault,
    charts.MaterialPalette.green.shadeDefault,
    charts.MaterialPalette.gray.shadeDefault,
    charts.MaterialPalette.indigo.shadeDefault,
    charts.MaterialPalette.lime.shadeDefault,
    charts.MaterialPalette.pink.shadeDefault,
    charts.MaterialPalette.teal.shadeDefault,
    charts.MaterialPalette.yellow.shadeDefault,
    charts.MaterialPalette.black
  ];

  List<Reading> get readings => List.from(readingsList);

  int get getSelectedReadingIndex => selectedReadingIndex;

  Future<dynamic> fetchIsolate(dynamic snapshot) async {
    ReceivePort receivePort = ReceivePort();

    await Isolate.spawn(fetchIsolateTask, receivePort.sendPort);

    SendPort sendPort = await receivePort.first;

    return await fetchIsolateChannel(sendPort, snapshot, colors);
  }

  Future<dynamic> fetchIsolateChannel(
      SendPort send, dynamic snapshot, List colors) {
    ReceivePort responsePort = ReceivePort();

    send.send([snapshot, responsePort.sendPort, colors]);
    return responsePort.first;
  }

  static fetchIsolateTask(SendPort sendPort) async {
    ReceivePort port = ReceivePort();

    sendPort.send(port.sendPort);

    await for (var msg in port) {
      dynamic snapshot = msg[0];
      SendPort replyPort = msg[1];
      List colors = msg[2];

      dynamic readingData = snapshot.value;

      num ahr = readingData['averageHeartRate'];

      Reading reading = Reading(
          id: snapshot.key,
          averageHeartRate: ahr.toDouble(),
          heartRate: (List.from(readingData['heartRate'])).cast<double>(),
          activity: readingData['activity'],
          moreInfo: readingData['moreInfo'],
          processed: readingData['processed'],
          timestamp: readingData['timestamp'],
          color: colors[Random().nextInt(colors.length - 1)],
          lat: readingData['latitude'],
          lon: readingData['longitude'],
          locale: readingData['locale']);
      replyPort.send(reading);
    }
  }

  Future<Null> fetchReadings(String id) {
    // Set loading variable for Spinners
    isLoading = true;
    notifyListeners();

    DatabaseReference userReportsRef =
        reportsRef.child(id).orderByChild('timestamp').reference();
    readingsList = [];
    Reading reading;
    int index;
    int sortIndex;
    num ahr;
    reportsSubscription.add(userReportsRef.onChildAdded.listen((Event event) {
      if (id == authenticatedUser.id) {
        ahr = event.snapshot.value['averageHeartRate'];
        reading = Reading(
            id: event.snapshot.key,
            averageHeartRate: ahr.toDouble(),
            heartRate:
                (List.from(event.snapshot.value['heartRate'])).cast<double>(),
            activity: event.snapshot.value['activity'],
            moreInfo: event.snapshot.value['moreInfo'],
            processed: event.snapshot.value['processed'],
            timestamp: event.snapshot.value['timestamp'],
            color: colors[Random().nextInt(colors.length - 1)],
            lat: event.snapshot.value['latitude'],
            lon: event.snapshot.value['longitude'],
            locale: event.snapshot.value['locale']);

        index = readingsList.indexWhere((item) => item.id == reading.id);
        sortIndex = readingsList
            .lastIndexWhere((item) => item.id.compareTo(reading.id) > 0);

        if (index == -1) {
          readingsList.insert(sortIndex + 1, reading);
          if (reading.processed) {
            avgHRByActivity[reading.activity] =
                (avgHRByActivity[reading.activity] *
                            numReportsByActivity[reading.activity] +
                        reading.averageHeartRate) /
                    (numReportsByActivity[reading.activity] + 1);
            numReportsByActivity[reading.activity]++;
          }
        }

        isLoading = false;
        notifyListeners();
      } else {
        fetchIsolate(event.snapshot).then((result) {
          reading = result;
          index = readingsList.indexWhere((item) => item.id == reading.id);
          sortIndex = readingsList
              .lastIndexWhere((item) => item.id.compareTo(reading.id) > 0);

          if (index == -1) {
            readingsList.insert(sortIndex + 1, reading);
            if (reading.processed) {
              avgHRByActivity[reading.activity] =
                  (avgHRByActivity[reading.activity] *
                              numReportsByActivity[reading.activity] +
                          reading.averageHeartRate) /
                      (numReportsByActivity[reading.activity] + 1);
              numReportsByActivity[reading.activity]++;
            }
          }

          isLoading = false;
          notifyListeners();
        });
      }
    }));

    reportsSubscription.add(userReportsRef.onChildRemoved.listen((Event event) {
      int index =
          readingsList.indexWhere((item) => item.id == event.snapshot.key);

      if (index >= 0) {
        readingsList.removeAt(index);

        numReportsByActivity[event.snapshot.value['activity']]--;

        if (numReportsByActivity[event.snapshot.value['activity']] == 0) {
          avgHRByActivity[event.snapshot.value['activity']] = 0;
        } else {
          avgHRByActivity[event.snapshot.value['activity']] = (avgHRByActivity[
                          event.snapshot.value['activity']] *
                      (numReportsByActivity[event.snapshot.value['activity']] +
                          1) -
                  event.snapshot.value['averageHeartRate']) /
              numReportsByActivity[event.snapshot.value['activity']];
        }
      }

      notifyListeners();
      
    }));

    reportsSubscription.add(userReportsRef.onChildChanged.listen((Event event) {
      if (event.snapshot.value['processed']) {
        num ahr = event.snapshot.value['averageHeartRate'];
        reading = Reading(
            id: event.snapshot.key,
            averageHeartRate: ahr.toDouble(),
            heartRate:
                (List.from(event.snapshot.value['heartRate'])).cast<double>(),
            activity: event.snapshot.value['activity'],
            moreInfo: event.snapshot.value['moreInfo'],
            processed: event.snapshot.value['processed'],
            timestamp: event.snapshot.value['timestamp'],
            color: colors[Random().nextInt(colors.length - 1)],
            lat: event.snapshot.value['latitude'],
            lon: event.snapshot.value['longitude'],
            locale: event.snapshot.value['locale']);

        index = readingsList.indexWhere((item) => item.id == reading.id);

        if (index >= 0) {
          readingsList.replaceRange(index, index + 1, [reading]);
          avgHRByActivity[reading.activity] =
              (avgHRByActivity[reading.activity] *
                          numReportsByActivity[reading.activity] +
                      reading.averageHeartRate) /
                  (numReportsByActivity[reading.activity] + 1);
          numReportsByActivity[reading.activity]++;
        }

        notifyListeners();
      }
    }));

    isLoading = false;
    notifyListeners();

    return Future<Null>.value();
  }

  void deleteReading() {
    final deletedReadingId = selectedReading.id;

    numReportsByActivity[selectedReading.activity]--;

    if (numReportsByActivity[selectedReading.activity] == 0) {
      avgHRByActivity[selectedReading.activity] = 0;
    } else {
      avgHRByActivity[selectedReading.activity] =
          (avgHRByActivity[selectedReading.activity] *
                      (numReportsByActivity[selectedReading.activity] + 1) -
                  selectedReading.averageHeartRate) /
              numReportsByActivity[selectedReading.activity];
    }

    readingsList.removeAt(selectedReadingIndex);

    selectedReadingIndex = null;

    commentsRef.child(deletedReadingId).reference().remove();
    videosRef.child(deletedReadingId).delete();

    reportsRef
        .child(authenticatedUser.id)
        .child(deletedReadingId)
        .reference()
        .remove();

    isLoading = false;
    notifyListeners();
  }

  void selectReading(int index) {
    selectedReadingIndex = index;
  }

  void addReading(Map<String, dynamic> readingData) {
    // Add the new reading to the app

    final Reading reading = Reading(
        id: readingData['id'],
        averageHeartRate: readingData['averageHeartRate'],
        heartRate: readingData['heartRate'],
        activity: readingData['activity'],
        moreInfo: readingData['moreInfo'],
        processed: readingData['processed'],
        timestamp: readingData['timestamp'],
        color: colors[Random().nextInt(colors.length - 1)],
        lat: readingData['latitude'],
        lon: readingData['longitude'],
        locale: readingData['locale']);

    int index = readingsList.indexWhere((item) => item.id == reading.id);

    int sortIndex = readingsList
        .lastIndexWhere((item) => item.id.compareTo(reading.id) > 0);

    if (index == -1) {
      readingsList.insert(sortIndex + 1, reading);
    }

    selectedReadingIndex = readingsList.length - 1;
  }
}
