Map<String, String> errorMessages = {
  "The password is invalid or the user does not have a password.":
      "Please check your password",
  "We have blocked all requests from this device due to unusual activity. Try again later. [ Too many unsuccessful login attempts.  Please include reCaptcha verification or try again later ]":
      "Too many failed attempts. Please try again later.",
  "There is no user record corresponding to this identifier. The user may have been deleted.":
      "Can't find the email.",
  "ERROR_WRONG_PASSWORD": "Please check again your credentials",
  "ERROR_INVALID_EMAIL": "Can't find the email",
  "ERROR_WRONG_PASSWORD": "Wrong password",
  "ERROR_USER_MISMATCH": "Please check again your credentials",
  "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL": "Account already exists",
  "ERROR_EMAIL_ALREADY_IN_USE": "Email already registered",
  "ERROR_CREDENTIAL_ALREADY_IN_USE": "Account already signed in",
  "ERROR_USER_DISABLED": "User is disabled",
  "ERROR_USER_TOKEN_EXPIRED": "Please sign in again",
  "ERROR_USER_NOT_FOUND": "Can't find the user",
  "ERROR_WEAK_PASSWORD": "Password is too weak",
  "ERROR_NETWORK_REQUEST_FAILED": "Please check your nternet connection"
};
