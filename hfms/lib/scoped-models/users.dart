import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/subjects.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './error_messages.dart';
import './connected_scope.dart';
import '../models/user.dart';
import 'package:connectivity/connectivity.dart';
import './utility.dart';
import 'package:local_auth/local_auth.dart';

mixin UsersModel on ConnectedScope {
  PublishSubject<bool> _userSubject = PublishSubject();
  PublishSubject<bool> get userSubject => _userSubject;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  /// authenticates (sign in) an user to the app
  /// Parameters:
  /// * user email
  /// * user password
  /// Returns:
  /// * Future with {bool: success, String: message}
  /// * * message contains the error message in case
  /// * * success is false and the authentication failed
  Future<Map<String, dynamic>> authenticate(
      String email, String password) async {
    isLoading = true;
    notifyListeners();

    String message = 'Please check your internet connection and try again';
    String title = 'Couldn\'t sign in';
    bool success = false;

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      isLoading = false;
      notifyListeners();
      success = false;
      message = 'Please check your internet connection.';
      return {'success': success, 'message': message, 'title': title};
    }

    final FirebaseUser user = await _auth
        .signInWithEmailAndPassword(email: email, password: password)
        .catchError((e) {
      success = false;
      message = errorMessages[e.message] == null ? e.message : errorMessages[e.message];
      return null;
    });

    TransactionResult transactionResult;
    if (user != null) {
      Map<String, dynamic> userData;
      String newUserUid = user.uid;

      connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.none) {
        isLoading = false;
        notifyListeners();
        success = false;
        message = 'Please check your internet connection.';
        return {'success': success, 'message': message, 'title': title};
      }

      String newUserToken = await user.getIdToken();

      transactionResult = await Future.any([
        usersRef
            .child(user.uid)
            .runTransaction((MutableData mutableData) async {
          if (mutableData != null) {
            userData = Map.from(mutableData.value);
            authenticatedUser = User(
                id: newUserUid,
                token: newUserToken,
                email: userData['email'],
                birthdate: userData['birthdate'],
                country: userData['country'],
                diseases: userData['diseases'],
                ethnicity: userData['ethnicity'],
                firstName: userData['firstName'],
                lastName: userData['lastName'],
                height: userData['height'],
                sex: userData['sex'],
                weight: userData['weight'],
                doctor: userData['doctor']);
          }
          return mutableData;
        }).catchError((e) {
          success = false;
          message = errorMessages[e.message] == null ? e.message : errorMessages[e.message];
          return null;
        }),
        Future.delayed(const Duration(seconds: 10))
      ]);

      if (userData != null) {
        _userSubject.add(true);
        final SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('userToken', newUserToken);
        prefs.setString('userEmail', email);
        prefs.setString('userId', newUserUid);
        prefs.setString('userBirthdate', userData['birthdate']);
        prefs.setString('userCountry', userData['country']);
        prefs.setString('userEthnicity', userData['ethnicity']);
        prefs.setString('userFirstName', userData['firstName']);
        prefs.setString('userLastName', userData['lastName']);
        prefs.setString('userHeight', userData['height']);
        prefs.setString('userSex', userData['sex']);
        prefs.setString('userWeight', userData['weight']);
        prefs.setBool('userDoctor', userData['doctor']);
        String diseasesPrefs = '';
        userData['diseases'].forEach((disease) {
          diseasesPrefs = ',$disease$diseasesPrefs';
        });

        diseasesPrefs = diseasesPrefs.substring(1);

        prefs.setString('userDiseases', diseasesPrefs);

        success = true;
        message = 'Authentication succeded!';
      } else {
        message = 'Please try again';
      }
    }

    isLoading = false;
    notifyListeners();
    return {'success': success, 'message': message, 'title': title};
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  ///
  Future<Map<String, dynamic>> signup(
      {String email,
      String password,
      String birthdate,
      String country,
      String height,
      String weight,
      String ethnicity,
      String firstName,
      String lastName,
      String sex,
      bool doctor,
      List<String> diseases}) async {
    isLoading = true;
    notifyListeners();

    bool success = false;
    String message = 'Something went wrong';

    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      isLoading = false;
      notifyListeners();
      success = false;
      message = 'Please check your internet connection.';
      return {'success': success, 'message': message};
    }

    final FirebaseUser user = await _auth
        .createUserWithEmailAndPassword(email: email, password: password)
        .catchError((e) {
      success = false;
      message = errorMessages[e.message] == null ? e.message : errorMessages[e.message];
      return null;
    });

    final Map<String, dynamic> userData = {
      'birthdate': birthdate,
      'country': country,
      'diseases': diseases,
      'email': email,
      'firstName': firstName,
      'sex': sex,
      'height': height,
      'lastName': lastName,
      'ethnicity': ethnicity,
      'weight': weight,
      'doctor': doctor
    };

    if (user != null) {
      String newUserUid = user.uid;
      String newUserToken = await user.getIdToken();
      DatabaseReference newUserRef = usersRef.child(newUserUid).reference();
      newUserRef.keepSynced(true);
      await newUserRef.set(userData).catchError((e) {
        success = false;
        message = errorMessages[e.message] == null ? e.message : errorMessages[e.message];
        return null;
      });

      success = true;
      message = 'Registration succeded!';
      authenticatedUser = User(
          id: newUserUid,
          email: email,
          token: newUserToken,
          birthdate: birthdate,
          country: country,
          diseases: diseases,
          ethnicity: ethnicity,
          firstName: firstName,
          lastName: lastName,
          height: height,
          sex: sex,
          weight: weight,
          doctor: doctor);
      _userSubject.add(true);
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('userToken', newUserToken);
      prefs.setString('userEmail', email);
      prefs.setString('userId', newUserUid);
      prefs.setString('userBirthdate', birthdate);
      prefs.setString('userCountry', country);
      prefs.setString('userEthnicity', ethnicity);
      prefs.setString('userFirstName', firstName);
      prefs.setString('userLastName', lastName);
      prefs.setString('userHeight', height);
      prefs.setString('userSex', sex);
      prefs.setString('userWeight', weight);
      prefs.setBool('userDoctor', doctor);
      String diseasesPrefs = '';
      diseases.forEach((disease) {
        diseasesPrefs = ',$disease$diseasesPrefs';
      });

      diseasesPrefs = diseasesPrefs.substring(1);

      prefs.setString('userDiseases', diseasesPrefs);
    }
    isLoading = false;
    notifyListeners();
    return {'success': success, 'message': message};
  }

  ///
  ///
  ///
  ///
  ///
  ///
  void autoAuthenticate() async {
    var localAuth = new LocalAuthentication();

    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String token = prefs.getString('userToken');

    if (token != null) {
      final String userEmail = prefs.getString('userEmail');
      final String userId = prefs.getString('userId');
      final String userBirthdate = prefs.getString('userBirthdate');
      final String userCountry = prefs.getString('userCountry');
      final String userEthnicity = prefs.getString('userEthnicity');
      final String userFirstName = prefs.getString('userFirstName');
      final String userLastName = prefs.getString('userLastName');
      final String userHeight = prefs.getString('userHeight');
      final String userSex = prefs.getString('userSex');
      final String userWeight = prefs.getString('userWeight');
      final bool userDoctor = prefs.getBool('userDoctor');
      List<String> userDiseases = prefs.getString('userDiseases').split(',');
      authenticatedUser = User(
          id: userId,
          email: userEmail,
          token: token,
          birthdate: userBirthdate,
          country: userCountry,
          diseases: userDiseases,
          ethnicity: userEthnicity,
          firstName: userFirstName,
          height: userHeight,
          lastName: userLastName,
          sex: userSex,
          weight: userWeight,
          doctor: userDoctor);
      _userSubject.add(true);
    }

    notifyListeners();
  }

  ///
  ///
  ///
  ///
  ///
  ///
  ///
  void logout() async {
    authenticatedUser = null;
    _userSubject.add(false);
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('userToken');
    prefs.remove('userEmail');
    prefs.remove('userId');
    prefs.remove('userBirthdate');
    prefs.remove('userCountry');
    prefs.remove('userEthnicity');
    prefs.remove('userFirstName');
    prefs.remove('userLastName');
    prefs.remove('userHeight');
    prefs.remove('userSex');
    prefs.remove('userWeight');
    prefs.remove('userDiseases');
    prefs.remove('userDoctor');
    prefs.remove('fingerprintLog');
    readingsList.clear();
    relationsList.clear();
    requestsList.clear();
    cancelSubscriptions();
    await _auth.signOut();
    isLoading = false;    
  }
}
