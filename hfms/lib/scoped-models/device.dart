import 'dart:async';
import 'dart:convert';
import 'package:date_format/date_format.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart' as geoloc;
import 'package:firebase_database/firebase_database.dart';
import 'dart:io';
import './connected_scope.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:convert' as JSON;

mixin DeviceModel on ConnectedScope {
  Future<String> _getAddress(double lat, double lng) async {
    final uri =
        'https://nominatim.openstreetmap.org/reverse?format=json&lat=$lat&lon=$lng&zoom=13';
    final http.Response response = await Future.any([
      http.get(uri).catchError((e) {
        print("ERROR!");
        return null;
      }),
      Future.delayed(Duration(seconds: 10), () {
        return null;
      })
    ]);
    if (response == null) {
      return null;
    }

    final decodedResponse = json.decode(response.body);
    print('TENGO MI LOCAL!');
    return '${decodedResponse['address']['city']}, ${decodedResponse['address']['country']}';
  }

  Future<geoloc.LocationData> _getLocation() async {
    geoloc.LocationData location;
    geoloc.Location _locationService = geoloc.Location();
    bool _permission = false;
    String _locationError;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      bool serviceStatus = await _locationService.serviceEnabled();
      print("Service status: $serviceStatus");
      if (serviceStatus) {
        _permission = await _locationService.requestPermission();
        print("Permission: $_permission");
        if (_permission) {
          location = await Future.any([
            _locationService.getLocation(),
            Future.delayed(Duration(seconds: 10), () {
              return null;
            })
          ]);
          if (location != null) {
            print("Location: ${location.latitude}, ${location.longitude}");
          }
        } else {
          location = null;
        }
      } else {
        bool serviceStatusResult = await _locationService.requestService();
        print("Service status activated after request: $serviceStatusResult");
        if (serviceStatusResult) {
          location = await _getLocation();
        }
      }
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        _locationError = e.message;
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        _locationError = e.message;
      }
      print('Error: $_locationError');
      location = null;
    }
    return location;
  }

  Future<Map<String, dynamic>> takeReading(
      File video, String activity, String moreInfo) async {
    // return Future.delayed(Duration(seconds: 5), () {
    //   return "1";
    // });

    videoUploadingCanceled = false;
    setPercentSendingVideo(0.0);

    // Location data

    String locale;
    double latitude;
    double longitude;

    if (!videoUploadingCanceled) {
      final geoloc.LocationData currentLocation = await _getLocation();

      setPercentSendingVideo(0.05);

      if (currentLocation != null) {
        locale = await _getAddress(
            currentLocation.latitude, currentLocation.longitude);
        latitude = currentLocation.latitude;
        longitude = currentLocation.longitude;
      }
    }

    setPercentSendingVideo(0.1);

    // All of the following data is burnt in

    List<double> heartRate = [1.3, 3.21, 2.1, 3.01, 2.78, 1.32];

    double averageHeartRate = 2.43;

    // Time is stamped at the end
    String timestamp = formatDate(DateTime.now(), dateFormat);

    Map<String, dynamic> newReading = {
      'averageHeartRate': averageHeartRate,
      'heartRate': heartRate,
      'activity': activity,
      'moreInfo': moreInfo,
      'processed': false,
      'timestamp': timestamp,
      'latitude': latitude,
      'longitude': longitude,
      'locale': locale
    };

    // SENDING READING

    DatabaseReference userReportsRef;
    DatabaseReference pushedReportRef;
    String newReadingId;

    if (!videoUploadingCanceled) {
      userReportsRef = reportsRef.child(authenticatedUser.id);

      // Add the new reading to the database
      pushedReportRef = userReportsRef.push();
      newReadingId = pushedReportRef.key;

      pushedReportRef.set(newReading);
    }

    // Create the new node in the database

    if (!videoUploadingCanceled) {
      print('Uploading...');

      videoUploading = videosRef.child(newReadingId).putFile(
            video,
            StorageMetadata(
              contentType: 'video/mp4',
            ),
          );

      int videoSize = video.readAsBytesSync().length;

      videoUploading.events.listen((StorageTaskEvent event) {
        setPercentSendingVideo(
            0.1 + (event.snapshot.bytesTransferred / videoSize) * 0.85);
      });

      await videoUploading.onComplete;

      print('Uploaded!');
    }

    if (videoUploading != null) {
      var response = await http.post(
          "https://video-receiver.herokuapp.com/api/videos/flutter/${authenticatedUser.id}/$newReadingId",
          headers: {'Content-Type': 'application/json'},
          body: JSON.jsonEncode({
            'videoUrl':
                "https://firebasestorage.googleapis.com/v0/b/heartu-ba7e0.appspot.com/o/videos%2F$newReadingId?alt=media"
          }));

      if (!videoUploadingCanceled) {
        setPercentSendingVideo(1.0);

        if (response.statusCode == 200) {
          print('Posted on server!');
        }
      }
    } else {
      if (pushedReportRef != null) {
        pushedReportRef.reference().remove();
        videosRef.child(newReadingId).delete();
      }      
      print('Canceled');
    }

    videoUploading = null;
    videoUploadingCanceled = false;

    return {
      'id': newReadingId,
      'timestamp': timestamp,
      'averageHeartRate': averageHeartRate,
      'heartRate': heartRate,
      'activity': activity,
      'moreInfo': moreInfo,
      'processed': false,
      'latitude': latitude,
      'longitude': longitude,
      'locale': locale
    };
  }
}
