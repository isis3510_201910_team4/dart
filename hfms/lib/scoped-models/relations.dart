import 'package:firebase_database/firebase_database.dart';
import './connected_scope.dart';
import '../models/user.dart';
import '../models/relation.dart';

mixin RelationsModel on ConnectedScope {
  List<User> get relations => List.from(relationsList);

  int get getSelectedRelationIndex => selectedRelationIndex;

  Future<TransactionResult> fetchRelations() {
    // Set loading variable for Spinners
    isLoading = true;
    notifyListeners();

    DatabaseReference userRelationsRef =
        relationsRef.child(authenticatedUser.id).reference();

    relationsSubscription
        .add(userRelationsRef.onChildAdded.listen((Event event) {
      dynamic relationData = event.snapshot.value;

      final Relation relation = Relation(
          id: event.snapshot.key,
          email: relationData['email'],
          firstName: relationData['firstName'],
          lastName: relationData['lastName']);

      if (relationData['accepted']) {
        int index = relationsList.indexWhere((item) => item.id == relation.id);

        if (index == -1 && event.snapshot.key != authenticatedUser.id) {
          relationsList.add(relation);
        }
      } else {
        int index = requestsList.indexWhere((item) => item.id == relation.id);

        if (index == -1 && event.snapshot.key != authenticatedUser.id) {
          requestsList.add(relation);
        }
      }

      notifyListeners();
    }));

    relationsSubscription
        .add(userRelationsRef.onChildRemoved.listen((Event event) {
      if (event.snapshot.value['accepted']) {
        relationsList.removeWhere((item) => item.id == event.snapshot.key);
      } else {
        requestsList.removeWhere((item) => item.id == event.snapshot.key);
      }

      notifyListeners();
    }));

    relationsSubscription
        .add(userRelationsRef.onChildChanged.listen((Event event) {
      if (event.snapshot.value['accepted']) {
        Map relationData = event.snapshot.value;

        final Relation relation = Relation(
            id: event.snapshot.key,
            email: relationData['email'],
            firstName: relationData['firstName'],
            lastName: relationData['lastName']);

        int start =
            relationsList.indexWhere((item) => item.id == event.snapshot.key);

        if (start < 0 && event.snapshot.key != authenticatedUser.id) {
          relationsList.add(relation);
        }

        requestsList.removeWhere((item) => item.id == event.snapshot.key);
        notifyListeners();
      }
    }));

    return userRelationsRef.runTransaction((MutableData mutableData) async {
      isLoading = false;
      notifyListeners();
      return mutableData;
    });
  }

  void deleteRelation() {
    // isLoading = true;
    // notifyListeners(); // This causes the list to show updating spinner every time you delete. Discuss with Nico. Not putting it makes it annoying to delete various readings quickly, putting it doesn't let the user do it quickly.

    String anotherUser = selectedRelation.id;

    relationsRef
        .child(authenticatedUser.id)
        .child(anotherUser)
        .reference()
        .remove();

    relationsRef
        .child(anotherUser)
        .child(authenticatedUser.id)
        .reference()
        .remove();

    relationsList.removeAt(selectedRelationIndex);
    selectedReadingIndex = null;

    isLoading = false;
    notifyListeners();
  }

  void rejectRelation(Relation relation) {
    relationsRef
        .child(authenticatedUser.id)
        .child(relation.id)
        .reference()
        .remove();

    relationsRef
        .child(relation.id)
        .child(authenticatedUser.id)
        .reference()
        .remove();

    requestsList.removeWhere((item) => item.id == relation.id);

    isLoading = false;
    notifyListeners();
  }

  Future<Null> selectRelation(int index) async {
    selectedRelationIndex = index;
    return usersRef
        .child(relationsList[selectedRelationIndex].id)
        .once()
        .then((DataSnapshot user) {
      Map userValues = user.value;
      User selectedUser = new User(
          id: userValues['id'],
          token: null,
          birthdate: userValues['birthdate'],
          country: userValues['country'],
          diseases: List.from(userValues['diseases']),
          email: userValues['email'],
          ethnicity: userValues['ethnicity'],
          firstName: userValues['firstName'],
          lastName: userValues['lastName'],
          height: userValues['height'],
          sex: userValues['sex'],
          weight: userValues['weight'],
          doctor: userValues['doctor']);
      relationUser = selectedUser;
    });
  }

  void acceptRelation(Relation relation) {
    isLoading = true;
    notifyListeners();

    relationsRef
        .child(authenticatedUser.id)
        .child(relation.id)
        .update({'accepted': true});

    relationsRef
        .child(relation.id)
        .child(authenticatedUser.id)
        .update({'accepted': true});

    isLoading = false;
    notifyListeners();
  }

  Future<int> addRelation(String userEmail) async {
    // Set loading variable for Spinners
    setLoading(true);

    // Create the new node in the database
    DatabaseReference userRelationsRef =
        relationsRef.child(authenticatedUser.id);

    /* This future will return -1 if the user with userEmail already exists in any of both lists (relations or requests),
    will return 0 if there is no any doctor user on firebase with that userEmail, and 1 if they exist and they are not in any list. */

    return usersRef
        .orderByChild('email')
        .equalTo(userEmail)
        .once()
        .then((DataSnapshot snapshot) {
      
      Map userTo = snapshot.value;

      if(userTo == null){
        setLoading(false);
        return 0;
      }

      String userId = userTo.keys.single;
      userTo = userTo[userId];   

      if(!userTo['doctor']){
        setLoading(false);
        return 0;
      }

      int indexWhereRelations =
          relationsList.indexWhere((item) => item.id == userId);
      int indexWhereRequests =
          requestsList.indexWhere((item) => item.id == userId);

      //Only add the new relation if it does not exist on relations or requests current lists

      if (indexWhereRelations == -1 && indexWhereRequests == -1) {
        // Add the new relation to the database
        DatabaseReference pushedRelationRef = userRelationsRef.child(userId);

        pushedRelationRef.set({
          'email': userTo['email'],
          'firstName': userTo['firstName'],
          'lastName': userTo['lastName'],
          'accepted': false
        });

        DatabaseReference pushedRelationPairRef =
            relationsRef.child(userId).child(authenticatedUser.id);
        pushedRelationPairRef.set({
          'email': authenticatedUser.email,
          'firstName': authenticatedUser.firstName,
          'lastName': authenticatedUser.lastName,
          'accepted': false
        });

        List<dynamic> diseasesList = List.from(userTo['diseases']);
        List<String> diseases = [];
        diseasesList.forEach((dynamic p) {
          diseases.add(p.toString());
        });

        // Add the new reading to the app
        final Relation relation = Relation(
            id: userId,
            email: userTo['email'],
            firstName: userTo['firstName'],
            lastName: userTo['lastName']);

        int index = requestsList.indexWhere((item) => item.id == relation.id);

        if (index == -1) {
          requestsList.add(relation);
        }
      } else {
        setLoading(false);
        return -1;
      }

      // Set loading variable for Spinners
      setLoading(false);
      return 1;
    });
  }
}
